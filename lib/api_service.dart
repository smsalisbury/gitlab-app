import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:gitlab/auth_service.dart';
import 'package:gitlab/issues/issue_model.dart';
import 'package:gitlab/merge_requests/merge_request_model.dart';
import 'package:gitlab/pipelines/pipeline_model.dart';
import 'package:gitlab/users/user_model.dart';
import 'package:http/http.dart' as http;
import 'package:gitlab/constants.dart';
import 'package:gitlab/projects/project_model.dart';

final Map<int, Project?> cachedProjects = {};

class PaginatedAPIList<T> {
  late List<T> data;
  late int total;

  PaginatedAPIList(this.data, this.total);

  factory PaginatedAPIList.empty() => PaginatedAPIList([], 0);
}

class PaginationData {
  final int page;
  final int perPage;
  final int? total;
  final int? totalPages;

  PaginationData({
    required this.page,
    required this.perPage,
    this.total,
    this.totalPages,
  });
}

class ApiService {
  static Future<dynamic> graphQl(String query) async {
    try {
      var url = Uri.https(ApiConstants.baseUrl, ApiConstants.graphQl);
      var response = await http.post(
        url,
        headers: {'Authorization': await AuthService.authHeader()},
        body: {
          'query': query,
        },
      );
      if (response.statusCode == 200) {
        return jsonDecode(response.body);
      }
    } catch (e) {
      print(e.toString());
    }
    return null;
  }

  static Future<String> renderMarkdown(String markdown,
      {gfm = true, String? projectPath}) async {
    try {
      var url = Uri.https(ApiConstants.baseUrl, 'api/v4/markdown');
      var response = await http.post(
        url,
        headers: {'Authorization': await AuthService.authHeader()},
        body: {
          'text': markdown,
          'gfm': gfm.toString(),
          'project': projectPath,
        }..removeWhere((key, value) => value == null),
      );
      if (response.statusCode == 201) {
        return jsonDecode(utf8.decode(response.bodyBytes))["html"];
      }
    } catch (e) {
      print(e.toString());
    }
    return markdown;
  }

  Future<CircleAvatar> getProjectAvatar(Project project) async {
    return project.avatarUrl != null
        ? CircleAvatar(
            foregroundImage: NetworkImage(project.avatarUrl ?? '',
                headers: {'Authorization': await AuthService.authHeader()}),
          )
        : CircleAvatar(
            child: Text(project.name[0].toUpperCase()),
          );
  }

  Future<User?> getUser() async {
    try {
      var url = Uri.https(ApiConstants.baseUrl, ApiConstants.user);
      var response = await http
          .get(url, headers: {'Authorization': await AuthService.authHeader()});
      if (response.statusCode == 200) {
        User model = userFromJson(response.body);
        return model;
      }
    } catch (e) {
      log(e.toString());
    }
    return null;
  }

  Future<List<Project>> getProjects(
      {bool starred = false,
      bool owned = false,
      bool membership = false}) async {
    try {
      var params = {
        'starred': starred.toString(),
        'owned': owned.toString(),
        'membership': membership.toString(),
        'order_by': 'last_activity_at',
        'sort': 'desc'
      };
      var url = Uri.https(ApiConstants.baseUrl, ApiConstants.projects, params);
      var response = await http
          .get(url, headers: {'Authorization': await AuthService.authHeader()});
      if (response.statusCode == 200) {
        List<Project> model = projectsFromJson(response.body);
        return model;
      }
    } catch (e) {
      log(e.toString());
    }
    return [];
  }

  Future<List<MergeRequest>> getMergeRequests(
      {String? scope, String? state}) async {
    try {
      var params = {
        'scope': scope,
        'state': state,
        'order_by': 'updated_at',
        'sort': 'desc'
      };
      var url =
          Uri.https(ApiConstants.baseUrl, ApiConstants.mergeRequests, params);
      var response = await http
          .get(url, headers: {'Authorization': await AuthService.authHeader()});
      if (response.statusCode == 200) {
        List<MergeRequest> model = mergeRequestsFromJson(response.body);
        return model;
      }
    } catch (e) {
      log(e.toString());
    }
    return [];
  }

  Future<Project?> getProject(int id, {bool cached = true}) async {
    if (cached && cachedProjects.containsKey(id)) return cachedProjects[id];
    try {
      var url = Uri.https(ApiConstants.baseUrl, '${ApiConstants.projects}/$id');
      var response = await http
          .get(url, headers: {'Authorization': await AuthService.authHeader()});
      if (response.statusCode == 200) {
        Project model = projectFromJson(response.body);
        cachedProjects[id] = model;
        return model;
      }
    } catch (e) {
      log(e.toString());
    }
    return null;
  }

  Future<List<Issue>> getIssues(
      {String? scope, String? myReactionEmoji, String? state}) async {
    try {
      var params = {
        'scope': scope,
        'state': state,
        'my_reaction_emoji': myReactionEmoji,
        'order_by': 'updated_at',
        'sort': 'desc'
      };
      var url = Uri.https(ApiConstants.baseUrl, ApiConstants.issues, params);
      var response = await http
          .get(url, headers: {'Authorization': await AuthService.authHeader()});
      if (response.statusCode == 200) {
        List<Issue> model = issuesFromJson(response.body);
        return model;
      }
    } catch (e) {
      print(e.toString());
    }
    return [];
  }

  Future<Pipeline?> getLatestPipeline(int projectId, {String? ref}) async {
    try {
      var params = {'ref': ref};
      var url = Uri.https(
        ApiConstants.baseUrl,
        ApiConstants.latestPipeline.replaceAll(':id', projectId.toString()),
        params,
      );
      var response = await http
          .get(url, headers: {'Authorization': await AuthService.authHeader()});
      if (response.statusCode == 200) {
        Pipeline model = pipelineFromJson(response.body);
        return model;
      }
    } catch (e) {
      log(e.toString());
    }
    return null;
  }
}

String urlTemplate(String url, Map<String, String> values) {
  values.forEach((key, value) => url = url.replaceAll(key, value));
  return url;
}
