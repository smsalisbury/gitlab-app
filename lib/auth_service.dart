import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_appauth/flutter_appauth.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:openidconnect/openidconnect.dart' as oid;

const domain = 'https://gitlab.com';

final authorizationEndpoint = Uri.parse('$domain/oauth/authorize');
final tokenEndpoint = Uri.parse('$domain/oauth/token');
final credentialsFile = File('~/.gitlab/credentials.json');
const callbackUrl = 'com.smsalisbury.gitit://oauth-callback';
const discoveryUrl = 'https://gitlab.com/.well-known/openid-configuration';

const storage = FlutterSecureStorage(
  aOptions: AndroidOptions(encryptedSharedPreferences: true),
);

Map<String, dynamic> tokenToJson(oid.AuthorizationResponse token) {
  return {
    'tokenType': token.tokenType,
    'expiresAt': token.expiresAt.toIso8601String(),
    'accessToken': token.accessToken,
    'refreshToken': token.refreshToken,
    'idToken': token.idToken,
  };
}

oid.AuthorizationResponse tokenFromJson(Map<String, dynamic> json) {
  return oid.AuthorizationResponse(
    accessToken: json['accessToken'],
    refreshToken: json['refreshToken'],
    expiresAt: DateTime.parse(json['expiresAt']),
    idToken: json['idToken'],
    tokenType: json['tokenType'],
  );
}

class AuthService {
  static login(BuildContext context) async {
    var config = await oid.OpenIdConnect.getConfiguration(discoveryUrl);
    inspect(config);
    if (!context.mounted) return;
    final result = await oid.OpenIdConnect.authorizeInteractive(
      context: context,
      title: 'Login',
      request: await oid.InteractiveAuthorizationRequest.create(
        clientId:
            '31a9c95a4678ecd14f8d899efca26d2bfa2ae1988df8ee30261d005e58ecebbe',
        clientSecret:
            '076d8d22d1ef87dc34939c85822df29b9cef203092eef9f5c64ef562cdb80bb5',
        redirectUrl: callbackUrl,
        scopes: ["openid", "api"],
        configuration: config,
        autoRefresh: true,
        useWebPopup: false,
      ),
    );
    inspect(result);

    if (result != null) {
      await storage.write(
          key: 'oauth', value: json.encode(tokenToJson(result)));
    }
  }

  static Future<String> authHeader() async {
    var token = await AuthService.getToken();
    if (token == null) return '';
    if (token.expiresAt.isBefore(DateTime.now())) {
      token = await AuthService.refresh();
      if (token == null) return '';
    }
    // TODO: get new token if expired
    return '${token.tokenType} ${token.accessToken}';
  }

  static Future<oid.AuthorizationResponse?> getToken() async {
    var tokenJson = await storage.read(key: 'oauth');
    if (tokenJson == null) return null;
    return tokenFromJson(json.decode(tokenJson));
  }

  static Future<oid.AuthorizationResponse?> refresh() async {
    var currentToken = await AuthService.getToken();
    if (currentToken == null) return null;

    FlutterAppAuth appAuth = const FlutterAppAuth();

    var config = await oid.OpenIdConnect.getConfiguration(discoveryUrl);
    var request = oid.RefreshRequest(
      clientId:
          '31a9c95a4678ecd14f8d899efca26d2bfa2ae1988df8ee30261d005e58ecebbe',
      clientSecret:
          '076d8d22d1ef87dc34939c85822df29b9cef203092eef9f5c64ef562cdb80bb5',
      refreshToken: currentToken.refreshToken!,
      scopes: ["openid", "api"],
      configuration: config,
      autoRefresh: true,
    );
    var result = await oid.OpenIdConnect.refreshToken(request: request);
    print('REFRESHED TOKEN');
    inspect(result);

    await storage.write(
      key: 'oauth',
      value: json.encode(tokenToJson(result)),
    );

    return result;
  }
}
