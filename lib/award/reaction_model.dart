import 'dart:convert';

import 'package:gitlab/users/user_model.dart';

Reaction reactionFromJson(String str) => Reaction.fromJson(json.decode(str));
List<Reaction> reactionsFromJson(String str) =>
    List<Reaction>.from(json.decode(str).map((x) => Reaction.fromJson(x)));

class Reaction {
  final int id;
  final String name;
  final User user;

  const Reaction({required this.id, required this.name, required this.user});

  factory Reaction.fromJson(Map<String, dynamic> json) => Reaction(
        id: json["id"],
        name: json["name"],
        user: User.fromJson(json['user']),
      );
}
