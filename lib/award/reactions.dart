import 'dart:developer';

import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_emoji/flutter_emoji.dart';
import 'package:gitlab/award/reaction_model.dart';
import 'package:gitlab/users/user_model.dart';
import 'package:material_symbols_icons/symbols.dart';

class Reactions extends StatefulWidget {
  final List<Reaction> reactions;
  final Future<Reaction?> Function(Reaction reaction)? onReact;

  const Reactions(this.reactions, {this.onReact, super.key});

  @override
  State<Reactions> createState() => _ReactionsState();
}

class _ReactionsState extends State<Reactions> {
  final _defaultEmojis = ['thumbsup', 'thumbsdown'];
  late final List<Reaction> _reactions = widget.reactions;
  late final Future<User?> _user = User.getMe();

  List<String> get _allEmojis {
    return [..._defaultEmojis, ..._customEmojis];
  }

  List<String> get _customEmojis {
    return _reactions
        .map((e) => e.name)
        .toSet()
        .where((e) => !_defaultEmojis.contains(e))
        .toList();
  }

  List<int> get _counts {
    return _allEmojis
        .map((e) =>
            _reactions.where((element) => element.name == e).toList().length)
        .toList();
  }

  List<Reaction?> _myReactions(User? user) {
    return _allEmojis.map((emoji) {
      try {
        return _reactions.firstWhere(
          (element) => element.user.id == user?.id && element.name == emoji,
        );
      } on StateError catch (_) {
        return null;
      }
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    var parser = EmojiParser();
    return FutureBuilder(
      future: _user,
      builder: (context, snapshot) {
        return snapshot.hasData
            ? Wrap(
                spacing: 8,
                children: [
                  ..._buildButtons(context, snapshot.data),
                  IconButton(
                    onPressed: () => showModalBottomSheet(
                      context: context,
                      builder: (context) => SizedBox(
                        height: 250,
                        child: EmojiPicker(
                          onEmojiSelected: (category, emoji) {
                            inspect(emoji);
                            _toggleReaction(parser.getEmoji(emoji.emoji).name,
                                snapshot.data);
                            Navigator.pop(context);
                          },
                          config: Config(
                            bgColor: Theme.of(context).dialogBackgroundColor,
                            enableSkinTones: false,
                            indicatorColor: Theme.of(context).primaryColor,
                            iconColorSelected: Theme.of(context).primaryColor,
                          ),
                        ),
                      ),
                    ),
                    style: OutlinedButton.styleFrom(
                      padding: EdgeInsets.zero,
                      visualDensity: VisualDensity.compact,
                    ),
                    icon: const Icon(Symbols.add_reaction),
                  )
                ],
              )
            : const SizedBox();
      },
    );
  }

  List<Widget> _buildButtons(BuildContext context, User? user) {
    return _allEmojis.map((emoji) {
      var index = _allEmojis.indexWhere((element) => element == emoji);
      var count = _counts[_allEmojis.indexOf(emoji)];
      var myReaction = index == -1 ? null : _myReactions(user)[index];
      return myReaction != null
          ? FilledButton.tonal(
              onPressed: () => _toggleReaction(emoji, user),
              style: FilledButton.styleFrom(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                visualDensity: VisualDensity.compact,
              ),
              child: _EmojiIcon(emoji, count),
            )
          : OutlinedButton(
              onPressed: () => _toggleReaction(emoji, user),
              style: OutlinedButton.styleFrom(
                side: BorderSide(
                    color: Theme.of(context).colorScheme.outlineVariant),
                padding: const EdgeInsets.symmetric(horizontal: 8),
                visualDensity: VisualDensity.compact,
              ),
              child: _EmojiIcon(emoji, count),
            );
    }).toList();
  }

  Future<void> _toggleReaction(String emoji, User? user) async {
    int index = _allEmojis.indexWhere((element) => element == emoji);
    var myReaction = index == -1 ? null : _myReactions(user)[index];
    if (widget.onReact == null) return;
    var saveReaction = myReaction ??
        Reaction(
          id: 0,
          name: emoji,
          user: user!,
        );
    var res = await widget.onReact!(saveReaction);
    setState(() {
      if (myReaction == null && res != null) {
        _reactions.add(res);
      } else {
        _reactions.removeWhere(
          (element) => element.name == emoji && user?.id == element.user.id,
        );
      }
    });
  }
}

class _EmojiIcon extends StatelessWidget {
  final String emoji;
  final int count;
  const _EmojiIcon(this.emoji, this.count);

  @override
  Widget build(BuildContext context) {
    var parser = EmojiParser();
    return IntrinsicWidth(
      child: Row(
        children: [
          Text(parser.get(emoji).code),
          if (count > 0) ...[
            const SizedBox(width: 4),
            Text(count.toString()),
          ],
        ],
      ),
    );
  }
}
