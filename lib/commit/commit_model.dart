import 'dart:convert';


Commit commitFromJson(String str) => Commit.fromJson(json.decode(str));
List<Commit> commitsFromJson(String str) =>
    List<Commit>.from(json.decode(str).map((x) => Commit.fromJson(x)));

class Commit {
  final String id;
  final String shortId;
  final String title;
  final String authorName;
  final String authorEmail;
  final DateTime createdAt;
  final String message;

  Commit({
    required this.id,
    required this.shortId,
    required this.title,
    required this.authorName,
    required this.authorEmail,
    required this.createdAt,
    required this.message,
  });

  factory Commit.fromJson(Map<String, dynamic> json) => Commit(
        id: json["id"],
        shortId: json["short_id"],
        title: json["title"],
        authorName: json["author_name"],
        authorEmail: json["author_email"],
        createdAt: DateTime.parse(json["created_at"]),
        message: json["message"],
      );
}
