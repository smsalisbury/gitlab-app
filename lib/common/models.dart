class References {
  final String short;
  final String relative;
  final String full;

  References({
    required this.short,
    required this.relative,
    required this.full,
  });

  factory References.fromJson(Map<String, dynamic> json) => References(
        short: json["short"],
        relative: json["relative"],
        full: json["full"],
      );
}
