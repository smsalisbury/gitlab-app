import 'package:flutter/material.dart';

class TitleText extends StatelessWidget {
  final String text;
  final double size;

  const TitleText({
    super.key,
    required this.text,
    this.size = 20,
  });

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: size,
      ),
    );
  }
}

class SubtitleText extends StatelessWidget {
  final String text;
  final double size;
  final TextStyle? style;

  const SubtitleText({
    super.key,
    required this.text,
    this.size = 12,
    this.style,
  });

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: (style ?? const TextStyle()).copyWith(
        color: Theme.of(context).hintColor,
        fontSize: size,
      ),
    );
  }
}

class CustomTextStyles {
  static final code = TextStyle(
    fontFamily: 'RobotoMono',
    backgroundColor: Colors.blueGrey.shade100,
  );
}

class InlineCodeText extends StatelessWidget {
  final String text;
  const InlineCodeText(this.text, {super.key});

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: CustomTextStyles.code,
    );
  }
}
