class ApiConstants {
  static String baseUrl = 'gitlab.com';
  static String graphQl = '/api/graphql';
  static String versionPath = '/api/v4';
  static String projects = '/api/v4/projects';
  static String issues = '/api/v4/issues';
  static String user = '/api/v4/user';
  static String mergeRequests = '/api/v4/merge_requests';
  static String latestPipeline = '$projects/:id/pipelines/latest';
}
