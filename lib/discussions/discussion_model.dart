import 'dart:convert';
import 'dart:developer';

import '../api_service.dart';
import '../auth_service.dart';
import '../constants.dart';
import '../issues/issue_model.dart';
import 'notes/note_model.dart';
import 'package:http/http.dart' as http;

var urlForIssue =
    '${ApiConstants.versionPath}/projects/:id/issues/:issue_iid/discussions';

Discussion discussionFromJson(String str) =>
    Discussion.fromJson(json.decode(str));
List<Discussion> discussionsFromJson(String str) =>
    List<Discussion>.from(json.decode(str).map((x) => Discussion.fromJson(x)));

class Discussion {
  bool individualNote;
  final String id;
  final List<Note> notes;

  Discussion({
    required this.id,
    required this.individualNote,
    required this.notes,
  });

  factory Discussion.fromJson(Map<String, dynamic> json) => Discussion(
        id: json["id"],
        individualNote: json["individual_note"],
        notes: notesFromJson(jsonEncode(json["notes"])),
      );

  static Future<PaginatedAPIList<Discussion>> forIssue(Issue issue,
      {int? page, int? perPage}) async {
    try {
      var params = {
        'sort': 'asc',
        'page': page?.toString(),
        'per_page': perPage?.toString(),
      };
      var url = Uri.https(
        ApiConstants.baseUrl,
        urlTemplate(
          urlForIssue,
          {
            ':id': issue.projectId.toString(),
            ':issue_iid': issue.iid.toString(),
          },
        ),
        params,
      );
      var response = await http
          .get(url, headers: {'Authorization': await AuthService.authHeader()});
      if (response.statusCode == 200) {
        List<Discussion> data =
            discussionsFromJson(utf8.decode(response.bodyBytes));
        return PaginatedAPIList<Discussion>(
          data,
          response.headers.containsKey('x-total')
              ? int.parse(response.headers['x-total']!)
              : data.length,
        );
      }
    } catch (e) {
      log(e.toString());
      rethrow;
    }
    return PaginatedAPIList.empty();
  }
}
