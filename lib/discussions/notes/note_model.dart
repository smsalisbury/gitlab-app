import 'dart:convert';
import 'dart:developer';

import '../../api_service.dart';
import '../../auth_service.dart';
import '../../constants.dart';
import '../../issues/issue_model.dart';
import '../../users/user_model.dart';
import 'package:http/http.dart' as http;

var urlForIssue =
    '${ApiConstants.versionPath}/projects/:id/issues/:issue_iid/notes';

Note noteFromJson(String str) => Note.fromJson(json.decode(str));
List<Note> notesFromJson(String str) =>
    List<Note>.from(json.decode(str).map((x) => Note.fromJson(x)));

class Note {
  final int id;
  final String body;
  final User author;
  final bool system;
  final DateTime createdAt;

  Note({
    required this.id,
    required this.body,
    required this.author,
    required this.system,
    required this.createdAt,
  });

  factory Note.fromJson(Map<String, dynamic> json) => Note(
        id: json["id"],
        body: json["body"],
        author: User.fromJson(json["author"]),
        system: json["system"],
        createdAt: DateTime.parse(json["created_at"]),
      );

  static Future<List<Note>> forIssue(Issue issue) async {
    try {
      var params = {
        'sort': 'asc',
      };
      var url = Uri.https(
        ApiConstants.baseUrl,
        urlTemplate(
          urlForIssue,
          {
            ':id': issue.projectId.toString(),
            ':issue_iid': issue.iid.toString(),
          },
        ),
        params,
      );
      var response = await http
          .get(url, headers: {'Authorization': await AuthService.authHeader()});
      if (response.statusCode == 200) {
        List<Note> model = notesFromJson(response.body);
        return model;
      }
    } catch (e) {
      log(e.toString());
      rethrow;
    }
    return [];
  }
}
