import 'package:flutter/material.dart';
import 'package:gitlab/users/widgets/avatar.dart';
import 'package:gitlab/widgets/markdown/gitlab_markdown.dart';
import 'package:intl/intl.dart';

import '../note_model.dart';

class NoteWidget extends StatelessWidget {
  final Note note;

  const NoteWidget({super.key, required this.note});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            UserAvatar(user: note.author, size: 24),
            const SizedBox(width: 8),
            Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 2),
                    child: Row(
                      children: [
                        Text(
                          note.author.name,
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(width: 4),
                        Text(DateFormat.jm().format(note.createdAt)),
                      ],
                    ),
                  ),
                  const SizedBox(height: 8),
                  GitLabMarkdown(note.body),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }
}
