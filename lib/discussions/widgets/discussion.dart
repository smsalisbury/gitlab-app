import 'package:flutter/material.dart';
import 'package:gitlab/discussions/discussion_model.dart';
import 'package:gitlab/discussions/notes/note_model.dart';
import 'package:gitlab/discussions/notes/widgets/note.dart';
import 'package:gitlab/users/widgets/avatar.dart';
import 'package:gitlab/widgets/markdown/gitlab_markdown.dart';
import 'package:gitlab/widgets/markdown/markdown_input.dart';
import 'package:timeago/timeago.dart' as timeago;

import 'list.dart';

class DiscussionWidget extends StatelessWidget {
  final Discussion discussion;
  final Future<void> Function(String value)? onReply;

  const DiscussionWidget({
    super.key,
    required this.discussion,
    this.onReply,
  });

  @override
  Widget build(BuildContext context) {
    Set<int> seenAuthors = {};
    var replyAuthors = discussion.notes
        .skip(1)
        .map((note) => note.author)
        .where((author) => seenAuthors.add(author.id))
        .toList();
    return GestureDetector(
      onTap: () => showModalBottomSheet(
        context: context,
        showDragHandle: true,
        isScrollControlled: true,
        useSafeArea: true,
        builder: (context) => Padding(
          padding:
              EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
          child: _DiscussionReplies(discussion, onReply),
        ),
      ),
      child: Column(
        children: [
          NoteWidget(note: discussion.notes[0]),
          if (discussion.notes.length > 1)
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Row(
                children: [
                  const SizedBox(width: 30),
                  Row(
                    children: replyAuthors
                        .map(
                          (author) => Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 2),
                            child: UserAvatar(user: author, size: 24),
                          ),
                        )
                        .toList(),
                  ),
                  const SizedBox(width: 4),
                  Text(
                    '${discussion.notes.length - 1} ${discussion.notes.length == 2 ? 'reply' : 'replies'}',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Theme.of(context).colorScheme.primary,
                    ),
                  ),
                  const SizedBox(width: 8),
                  Text(timeago
                      .format(discussion.notes.last.createdAt.toLocal())),
                ],
              ),
            ),
        ],
      ),
    );
  }
}

class _DiscussionReplies extends StatefulWidget {
  final Discussion discussion;
  final Future<void> Function(String value)? onReply;

  const _DiscussionReplies(this.discussion, this.onReply);

  @override
  State<_DiscussionReplies> createState() => _DiscussionRepliesState();
}

class _DiscussionRepliesState extends State<_DiscussionReplies> {
  bool _replying = false;

  Note get note {
    return widget.discussion.notes[0];
  }

  List<Note> get replies {
    return widget.discussion.notes.skip(1).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Flexible(
          child: SingleChildScrollView(
            child: Column(
              children: [
                ListTile(
                  leading: UserAvatar(user: note.author),
                  title: Text(note.author.name),
                  subtitle: Text(timeago.format(note.createdAt.toLocal())),
                ),
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: GitLabMarkdown(note.body),
                ),
                _buildReplies(),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(16),
          child: !_replying
              ? GestureDetector(
                  onTap: () => setState(() {
                    _replying = true;
                  }),
                  child: const TextField(
                    enabled: false,
                    decoration: InputDecoration(
                      hintText: 'Reply...',
                      border: OutlineInputBorder(),
                    ),
                  ),
                )
              : MarkdownField(
                  autofocus: true,
                  onSubmit: (value) async {
                    if (widget.onReply != null) await widget.onReply!(value);
                    if (context.mounted) Navigator.pop(context);
                  },
                ),
        ),
      ],
    );
  }

  Widget _buildReplies() {
    return ListView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: replies.length,
      itemBuilder: (context, index) {
        var previousDate = index > 0 ? replies[index - 1].createdAt : null;
        var date = replies[index].createdAt;
        return Column(
          children: [
            if (!DateDivider.isSameDate(previousDate, date)) DateDivider(date),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 16),
              child: NoteWidget(note: replies[index]),
            ),
          ],
        );
      },
    );
  }
}
