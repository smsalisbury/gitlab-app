import 'package:flutter/material.dart';
import 'package:gitlab/discussions/widgets/discussion.dart';
import 'package:gitlab/issues/issue_model.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:intl/intl.dart';

import '../discussion_model.dart';

class DiscussionList extends StatefulWidget {
  final Issue issue;

  const DiscussionList({super.key, required this.issue});

  @override
  State<DiscussionList> createState() => _DiscussionListState();
}

class _DiscussionListState extends State<DiscussionList> {
  static const pageSize = 20;
  final PagingController<int, Discussion> pagingController =
      PagingController(firstPageKey: 0);
  final dateFormat = DateFormat('MMMM d');

  @override
  void initState() {
    super.initState();
    getData();
  }

  void getData() async {
    pagingController.addPageRequestListener((pageKey) => fetchPage(pageKey));
    setState(() {});
  }

  Future<void> fetchPage(int pageKey) async {
    try {
      var page = (pageKey / pageSize).floor() + 1;
      var list = await Discussion.forIssue(widget.issue,
          page: page, perPage: pageSize);
      final isLastPage =
          pageKey + list.data.length == list.total || pageKey > 30;
      var nonSystemDiscussions = list.data
          .where(
            (element) =>
                !(element.notes.length == 1 && element.notes[0].system),
          )
          .toList();
      if (isLastPage) {
        pagingController.appendLastPage(nonSystemDiscussions);
      } else {
        final nextPageKey = pageKey + list.data.length;
        pagingController.appendPage(nonSystemDiscussions, nextPageKey);
      }
    } catch (error) {
      pagingController.error = error;
    }
  }

  @override
  Widget build(BuildContext context) {
    return PagedListView(
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      pagingController: pagingController,
      builderDelegate: PagedChildBuilderDelegate<Discussion>(
        itemBuilder: (context, item, index) {
          var previousDate = index > 0
              ? pagingController.itemList![index - 1].notes[0].createdAt
              : null;
          var date = item.notes[0].createdAt;
          return Column(
            children: [
              if (!DateDivider.isSameDate(previousDate, date))
                DateDivider(date),
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 4, horizontal: 16),
                child: DiscussionWidget(
                  discussion: item,
                  onReply: (value) async {
                    await widget.issue.replyToDiscussion(item, value);
                    pagingController.refresh();
                  },
                ),
              ),
            ],
          );
        },
      ),
    );
    // TODO: fixed comment bar at bottom
  }
}

class DateDivider extends StatelessWidget {
  final DateTime date;
  static DateFormat format = DateFormat('MMMM d');

  const DateDivider(this.date, {super.key});

  static bool isSameDate(DateTime? a, DateTime? b) {
    return a != null && b != null && format.format(a) == format.format(b);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Row(
        children: [
          const Flexible(child: Divider()),
          Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: Theme.of(context).colorScheme.outlineVariant,
                width: 1,
              ),
              borderRadius: BorderRadius.circular(12),
            ),
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
            child: Text(format.format(date)),
          ),
          const Flexible(child: Divider()),
        ],
      ),
    );
  }
}
