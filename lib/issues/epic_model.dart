import 'dart:convert';
import 'package:gitlab/issues/issue_model.dart';
import 'package:http/http.dart' as http;

import '../api_service.dart';
import '../auth_service.dart';
import '../constants.dart';

Epic epicFromJson(String str) => Epic.fromJson(json.decode(str));
List<Epic> epicsFromJson(String str) =>
    List<Epic>.from(json.decode(str).map((x) => Epic.fromJson(x)));
List<EpicIssue> epicIssuesFromJson(String str, Epic epic) =>
    List<EpicIssue>.from(
        json.decode(str).map((x) => EpicIssue.fromJson(x, epic)));

class _Urls {
  static final listForGroup = '${ApiConstants.versionPath}/groups/:id/epics';
  static final listIssues =
      '${ApiConstants.versionPath}/groups/:id/epics/:epic_iid/issues';
  static final assignIssueToEpic =
      '${ApiConstants.versionPath}/groups/:id/epics/:epic_iid/issues/:issue_id';
  static final removeIssueFromEpic =
      '${ApiConstants.versionPath}/groups/:id/epics/:epic_iid/issues/:epic_issue_id';
}

class Epic {
  final int id;
  final int iid;
  final String title;
  final int groupId;
  final String? humanReadableEndDate;
  final String? humanReadableTimestamp;

  const Epic({
    required this.id,
    required this.iid,
    required this.title,
    required this.groupId,
    this.humanReadableEndDate,
    this.humanReadableTimestamp,
  });

  factory Epic.fromJson(Map<String, dynamic> json) => Epic(
        id: json["id"],
        iid: json["iid"],
        title: json["title"],
        groupId: json["group_id"],
        humanReadableEndDate: json["human_readable_end_date"],
        humanReadableTimestamp: json["human_readable_timestamp"],
      );

  static Future<PaginatedAPIList<Epic>> listForGroup(
    int groupId, {
    String? search,
    int? page,
    int? perPage,
  }) async {
    try {
      var params = {
        'search': search,
        'order_by': 'updated_at',
        'sort': 'desc',
        'page': page?.toString(),
        'per_page': perPage?.toString(),
      };
      var url = Uri.https(
        ApiConstants.baseUrl,
        urlTemplate(_Urls.listForGroup, {':id': groupId.toString()}),
        params,
      );
      var response = await http
          .get(url, headers: {'Authorization': await AuthService.authHeader()});
      if (response.statusCode == 200) {
        List<Epic> data = epicsFromJson(utf8.decode(response.bodyBytes));
        return PaginatedAPIList<Epic>(
          data,
          response.headers.containsKey('x-total')
              ? int.parse(response.headers['x-total']!)
              : data.length,
        );
      }
    } catch (e) {
      print(e.toString());
    }
    return PaginatedAPIList.empty();
  }

  Future<List<EpicIssue>> listIssues() async {
    try {
      var url = Uri.https(
        ApiConstants.baseUrl,
        urlTemplate(_Urls.listIssues, {
          ':id': groupId.toString(),
          ':epic_iid': iid.toString(),
        }),
      );
      var response = await http
          .get(url, headers: {'Authorization': await AuthService.authHeader()});
      if (response.statusCode == 200) {
        List<EpicIssue> data = epicIssuesFromJson(response.body, this);
        return data;
      }
    } catch (e) {
      print(e.toString());
    }
    return [];
  }

  Future<EpicIssue?> getFromIssue(Issue issue) async {
    var epicIssues = await listIssues();
    return epicIssues.firstWhere((i) => i.id == issue.id);
  }

  Future<EpicIssueAssociation?> addIssue(Issue issue) async {
    return EpicIssueAssociation.create(this, issue);
  }

  Future<void> removeIssue(Issue issue) async {
    var epicIssue = await getFromIssue(issue);
    if (epicIssue == null) return;
    await epicIssue.delete();
  }
}

class EpicIssue {
  final int id;
  final int iid;
  final int epicIssueId;
  final Epic epic;

  EpicIssue({
    required this.id,
    required this.iid,
    required this.epicIssueId,
    required this.epic,
  });

  factory EpicIssue.fromJson(Map<String, dynamic> json, Epic epic) => EpicIssue(
        id: json["id"],
        iid: json["iid"],
        epicIssueId: json["epic_issue_id"],
        epic: epic,
      );

  Future<void> delete() async {
    try {
      var url = Uri.https(
        ApiConstants.baseUrl,
        urlTemplate(_Urls.removeIssueFromEpic, {
          ':id': epic.groupId.toString(),
          ':epic_iid': epic.iid.toString(),
          ':epic_issue_id': epicIssueId.toString(),
        }),
      );
      await http.delete(url,
          headers: {'Authorization': await AuthService.authHeader()});
    } catch (e) {
      print(e.toString());
    }
  }
}

class EpicIssueAssociation {
  final int id;
  final Epic epic;
  final Issue issue;

  const EpicIssueAssociation({
    required this.id,
    required this.epic,
    required this.issue,
  });

  factory EpicIssueAssociation.fromJson(Map<String, dynamic> json) =>
      EpicIssueAssociation(
        id: json["id"],
        epic: Epic.fromJson(json['epic']),
        issue: Issue.fromJson(json['issue']),
      );

  static Future<EpicIssueAssociation?> create(
    Epic epic,
    Issue issue,
  ) async {
    try {
      var url = Uri.https(
        ApiConstants.baseUrl,
        urlTemplate(_Urls.assignIssueToEpic, {
          ':id': epic.groupId.toString(),
          ':epic_iid': epic.iid.toString(),
          ':issue_id': issue.id.toString(),
        }),
      );
      var response = await http.post(url,
          headers: {'Authorization': await AuthService.authHeader()});
      if (response.statusCode == 200) {
        EpicIssueAssociation data =
            EpicIssueAssociation.fromJson(json.decode(response.body));
        return data;
      }
    } catch (e) {
      print(e.toString());
    }
    return null;
  }
}
