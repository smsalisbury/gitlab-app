import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:gitlab/api_service.dart';
import 'package:gitlab/award/reaction_model.dart';
import 'package:gitlab/discussions/notes/note_model.dart';
import 'package:gitlab/planning/milestones/milestone_model.dart';
import 'package:gitlab/users/user_model.dart';
import 'package:http/http.dart' as http;

import '../auth_service.dart';
import '../common/models.dart';
import '../constants.dart';
import '../discussions/discussion_model.dart';
import 'epic_model.dart';

Issue issueFromJson(String str) => Issue.fromJson(json.decode(str));
List<Issue> issuesFromJson(String str) =>
    List<Issue>.from(json.decode(str).map((x) => Issue.fromJson(x)));

final Map<String, Issue?> cachedIssues = {};

class _Urls {
  static final singleIssue =
      '${ApiConstants.versionPath}/projects/:project/issues/:iid';
  static final awards =
      '${ApiConstants.versionPath}/projects/:project/issues/:iid/award_emoji';
  static final participants =
      '${ApiConstants.versionPath}/projects/:project/issues/:iid/participants';
  static final subscribe =
      '${ApiConstants.versionPath}/projects/:project/issues/:iid/subscribe';
  static final unsubscribe =
      '${ApiConstants.versionPath}/projects/:project/issues/:iid/unsubscribe';
  static final discussions =
      '${ApiConstants.versionPath}/projects/:project/issues/:iid/discussions';
  static final notes =
      '${ApiConstants.versionPath}/projects/:project/issues/:iid/notes';
  static final discussionNotes =
      '${ApiConstants.versionPath}/projects/:project/issues/:iid/discussions/:discussion/notes';
}

class Issue {
  final int id;
  final int iid;
  final String title;
  final int projectId;
  final User author;
  final List<User> assignees;
  final List<String> labels;
  final DateTime createdAt;
  final String? description;
  final Milestone? milestone;
  final int? weight;
  final Epic? epic;
  final bool confidential;
  final References references;
  final String state;
  final IssueFeatures? features;
  final bool? subscribed;
  final bool? discussionLocked;

  Issue({
    required this.id,
    required this.iid,
    required this.title,
    required this.projectId,
    required this.references,
    required this.author,
    required this.assignees,
    required this.labels,
    required this.createdAt,
    required this.state,
    required this.confidential,
    this.description,
    this.milestone,
    this.weight,
    this.epic,
    this.features,
    this.subscribed,
    this.discussionLocked,
  });

  factory Issue.fromJson(Map<String, dynamic> json) => Issue(
        id: json["id"],
        iid: json["iid"],
        title: json["title"],
        projectId: json["project_id"],
        author: User.fromJson(json['author']),
        assignees: usersFromJson(jsonEncode(json['assignees'])),
        labels: List<String>.from(json['labels']),
        milestone: json["milestone"] != null
            ? Milestone.fromJson(json["milestone"])
            : null,
        weight: json["weight"],
        references: References.fromJson(json['references']),
        createdAt: DateTime.parse(json["created_at"]),
        state: json["state"],
        description: json["description"],
        epic: json["epic"] == null ? null : Epic.fromJson(json["epic"]),
        confidential: json["confidential"],
        subscribed: json["subscribed"],
        discussionLocked: json["discussion_locked"],
        features: IssueFeatures(
          epic: json.containsKey('epic'),
          weight: json.containsKey('weight'),
        ),
      );

  static Future<PaginatedAPIList<Issue>> list({
    String? scope,
    String? myReactionEmoji,
    String state = 'opened',
    int? page,
    int? perPage,
  }) async {
    try {
      var params = {
        'scope': scope,
        'state': state,
        'my_reaction_emoji': myReactionEmoji,
        'order_by': 'updated_at',
        'sort': 'desc',
        'page': page?.toString(),
        'per_page': perPage?.toString(),
      };
      var url = Uri.https(ApiConstants.baseUrl, ApiConstants.issues, params);
      var response = await http
          .get(url, headers: {'Authorization': await AuthService.authHeader()});
      if (response.statusCode == 200) {
        List<Issue> data = issuesFromJson(utf8.decode(response.bodyBytes));
        return PaginatedAPIList<Issue>(
          data,
          response.headers.containsKey('x-total')
              ? int.parse(response.headers['x-total']!)
              : data.length,
        );
      } else if (response.statusCode >= 500) {
        throw response;
      }
    } catch (e) {
      print(e.toString());
      rethrow;
    }
    return PaginatedAPIList.empty();
  }

  Future<Issue?> reload() async {
    return Issue.get(iid, projectId: projectId);
  }

  static Future<Issue?> get(
    int iid, {
    String? projectPath,
    int? projectId,
    bool cached = true,
  }) async {
    assert(
      projectId != null || projectPath != null,
      'One of id or path must be specified',
    );

    var project = projectId?.toString() ?? Uri.encodeComponent(projectPath!);
    var cacheKey = '$project-$iid';
    if (cached && cachedProjects.containsKey(project)) {
      return cachedIssues[cacheKey];
    }

    try {
      var url = Uri(
        scheme: 'https',
        host: ApiConstants.baseUrl,
        path: urlTemplate(
          _Urls.singleIssue,
          {':project': project, ':iid': iid.toString()},
        ),
      );
      var response = await http
          .get(url, headers: {'Authorization': await AuthService.authHeader()});
      if (response.statusCode == 200) {
        Issue model = issueFromJson(response.body);
        cachedIssues[cacheKey] = model;
        return model;
      }
    } catch (e) {
      log(e.toString());
    }

    return null;
  }

  Future<void> subscribe() async {
    try {
      var url = Uri(
        scheme: 'https',
        host: ApiConstants.baseUrl,
        path: urlTemplate(
          _Urls.subscribe,
          {':project': projectId.toString(), ':iid': iid.toString()},
        ),
      );
      await http.post(
        url,
        headers: {
          HttpHeaders.authorizationHeader: await AuthService.authHeader(),
        },
      );
    } catch (e) {
      print(e.toString());
    }
  }

  Future<void> unsubscribe() async {
    try {
      var url = Uri(
        scheme: 'https',
        host: ApiConstants.baseUrl,
        path: urlTemplate(
          _Urls.unsubscribe,
          {':project': projectId.toString(), ':iid': iid.toString()},
        ),
      );
      await http.post(
        url,
        headers: {
          HttpHeaders.authorizationHeader: await AuthService.authHeader(),
        },
      );
    } catch (e) {
      print(e.toString());
    }
  }

  Future<List<User>> participants() async {
    try {
      var url = Uri(
        scheme: 'https',
        host: ApiConstants.baseUrl,
        path: urlTemplate(
          _Urls.participants,
          {':project': projectId.toString(), ':iid': iid.toString()},
        ),
      );
      var response = await http.get(
        url,
        headers: {
          HttpHeaders.authorizationHeader: await AuthService.authHeader(),
        },
      );
      if (response.statusCode == 200) {
        return usersFromJson(response.body);
      }
    } catch (e) {
      print(e.toString());
    }
    return [];
  }

  Future<Issue?> update({
    List<int>? assigneeIds,
    int? milestoneId,
    List<String>? labels,
    int? weight,
    bool? confidential,
    bool? discussionLocked,
    String? stateEvent,
    String? title,
    String? description,
  }) async {
    try {
      var body = {
        'assignee_ids': assigneeIds,
        'milestone_id': milestoneId,
        'labels': labels,
        'weight': weight,
        'confidential': confidential,
        'discussion_locked': discussionLocked,
        'state_event': stateEvent,
        'title': title,
        'description': description,
      }..removeWhere((key, value) => value == null);
      var url = Uri(
        scheme: 'https',
        host: ApiConstants.baseUrl,
        path: urlTemplate(
          _Urls.singleIssue,
          {':project': projectId.toString(), ':iid': iid.toString()},
        ),
      );
      var response = await http.put(
        url,
        headers: {
          HttpHeaders.authorizationHeader: await AuthService.authHeader(),
          HttpHeaders.contentTypeHeader: 'application/json'
        },
        body: jsonEncode(body),
      );
      if (response.statusCode == 200) {
        Issue model = issueFromJson(response.body);
        return model;
      }
    } catch (e) {
      print(e.toString());
    }

    return null;
  }

  Future<Issue?> removeWeight() async {
    try {
      var url = Uri(
        scheme: 'https',
        host: ApiConstants.baseUrl,
        path: urlTemplate(
          _Urls.singleIssue,
          {':project': projectId.toString(), ':iid': iid.toString()},
        ),
      );
      var response = await http.put(
        url,
        headers: {
          HttpHeaders.authorizationHeader: await AuthService.authHeader(),
          HttpHeaders.contentTypeHeader: 'application/json'
        },
        body: jsonEncode({'weight': null}),
      );
      if (response.statusCode == 200) {
        Issue model = issueFromJson(response.body);
        return model;
      }
    } catch (e) {
      print(e.toString());
    }

    return null;
  }

  Future<List<Reaction>> awards() async {
    late int pages;
    Future<List<Reaction>> makeRequest({int page = 1}) async {
      var url = Uri(
          scheme: 'https',
          host: ApiConstants.baseUrl,
          path: urlTemplate(
            _Urls.awards,
            {':project': projectId.toString(), ':iid': iid.toString()},
          ),
          queryParameters: {
            'page': page.toString(),
          });
      var response = await http
          .get(url, headers: {'Authorization': await AuthService.authHeader()});
      if (response.statusCode == 200) {
        pages = int.parse(response.headers['x-total-pages'] ?? '1');
        List<Reaction> model = reactionsFromJson(response.body);
        return model;
      }
      return [];
    }

    try {
      List<Reaction> data = await makeRequest();
      if (pages > 1) {
        var pageList = List<int>.generate(pages - 1, (index) => index + 2);
        var pagedData =
            await Future.wait(pageList.map((page) => makeRequest(page: page)));
        data.addAll(pagedData.expand((element) => element));
      }
      return data;
    } catch (e) {
      log(e.toString());
    }

    return [];
  }

  static Future<Reaction?> award(Issue issue, String name) async {
    try {
      var url = Uri(
        scheme: 'https',
        host: ApiConstants.baseUrl,
        path: urlTemplate(
          _Urls.awards,
          {
            ':project': issue.projectId.toString(),
            ':iid': issue.iid.toString()
          },
        ),
        queryParameters: {"name": name},
      );
      var response = await http.post(url, headers: {
        'Authorization': await AuthService.authHeader(),
      });
      if (response.statusCode == 201) {
        Reaction model = reactionFromJson(response.body);
        return model;
      }
    } catch (e) {
      log(e.toString());
    }

    return null;
  }

  static Future<void> unaward(Issue issue, Reaction reaction) async {
    try {
      var url = Uri(
        scheme: 'https',
        host: ApiConstants.baseUrl,
        path: urlTemplate(
          "${_Urls.awards}/:award_id",
          {
            ':project': issue.projectId.toString(),
            ':iid': issue.iid.toString(),
            ':award_id': reaction.id.toString(),
          },
        ),
      );
      await http.delete(url, headers: {
        'Authorization': await AuthService.authHeader(),
      });
    } catch (e) {
      log(e.toString());
    }
  }

  Future<Note?> comment(String content, {bool internal = false}) async {
    try {
      var url = Uri(
        scheme: 'https',
        host: ApiConstants.baseUrl,
        path: urlTemplate(
          _Urls.notes,
          {
            ':project': projectId.toString(),
            ':iid': iid.toString(),
          },
        ),
        queryParameters: {
          "body": content,
          "internal": internal.toString(),
        },
      );
      var response = await http.post(url, headers: {
        'Authorization': await AuthService.authHeader(),
      });
      if (response.statusCode == 200) {
        Note model = noteFromJson(response.body);
        return model;
      }
    } catch (e) {
      log(e.toString());
    }

    return null;
  }

  Future<Note?> replyToDiscussion(Discussion discussion, String content) async {
    try {
      var url = Uri(
        scheme: 'https',
        host: ApiConstants.baseUrl,
        path: urlTemplate(
          _Urls.discussionNotes,
          {
            ':project': projectId.toString(),
            ':iid': iid.toString(),
            ':discussion': discussion.id.toString(),
          },
        ),
        queryParameters: {"body": content},
      );
      var response = await http.post(url, headers: {
        'Authorization': await AuthService.authHeader(),
      });
      if (response.statusCode == 200) {
        Note model = noteFromJson(response.body);
        return model;
      }
    } catch (e) {
      log(e.toString());
    }

    return null;
  }
}

class IssueFeatures {
  final bool epic;
  final bool weight;

  const IssueFeatures({required this.epic, required this.weight});
}
