import 'package:flutter/material.dart';
import 'package:gitlab/award/reaction_model.dart';
import 'package:gitlab/award/reactions.dart';
import 'package:gitlab/issues/widgets/detail/drawer/drawer.dart';
import 'package:gitlab/issues/widgets/detail/header.dart';
import 'package:gitlab/widgets/markdown/gitlab_markdown.dart';
import 'package:gitlab/discussions/widgets/list.dart';
import 'package:gitlab/widgets/markdown/markdown_input.dart';

import '../../issue_model.dart';

class IssueDetail extends StatefulWidget {
  final Issue? issue;
  final int? iid;
  final String? projectPath;
  final int? projectId;

  const IssueDetail({
    super.key,
    this.issue,
    this.iid,
    this.projectPath,
    this.projectId,
  }) : assert(issue != null ||
            (iid != null && (projectPath != null || projectId != null)));

  @override
  State<IssueDetail> createState() => _IssueDetailState();

  String get project {
    return projectId?.toString() ?? Uri.encodeComponent(projectPath!);
  }
}

class _IssueDetailState extends State<IssueDetail> {
  late Future<Issue?> _issue;

  @override
  void initState() {
    getData();
    super.initState();
  }

  void getData() {
    setState(() {
      _issue = widget.issue != null
          ? widget.issue!.reload()
          : Issue.get(
              widget.iid!,
              projectId: widget.projectId,
              projectPath: widget.projectPath,
            );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.issue == null
            ? '#${widget.iid.toString()}'
            : widget.issue!.references.short),
        actions: [
          Builder(
            builder: (context) => IconButton(
              onPressed: () => Scaffold.of(context).openEndDrawer(),
              icon: const Icon(Icons.edit),
            ),
          ),
        ],
      ),
      endDrawer: FutureBuilder(
        future: _issue,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return IssueDrawer(
              snapshot.data!,
              onChanged: () => getData(),
            );
          }
          return const Drawer(
            child: Center(child: CircularProgressIndicator()),
          );
        },
      ),
      body: FutureBuilder(
        future: _issue,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done &&
              snapshot.hasData) {
            var issue = snapshot.data;
            return RefreshIndicator(
              onRefresh: () => Future.sync(() => getData()),
              child: SingleChildScrollView(
                physics: const AlwaysScrollableScrollPhysics(),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          IssueDetailHeader(issue!),
                          if (issue.description != null)
                            GitLabMarkdown(
                              issue.description!,
                              projectId: issue.projectId,
                              projectPath: widget.projectPath,
                            ),
                          const SizedBox(height: 8),
                          FutureBuilder(
                            future: issue.awards(),
                            builder: (context, snapshot) {
                              if (snapshot.connectionState ==
                                      ConnectionState.done &&
                                  snapshot.hasData) {
                                return Reactions(
                                  snapshot.data!,
                                  onReact: (reaction) async {
                                    if (reaction.id == 0) {
                                      return await Issue.award(
                                        widget.issue!,
                                        reaction.name,
                                      );
                                    } else {
                                      await Issue.unaward(
                                          widget.issue!, reaction);
                                    }
                                  },
                                );
                              }
                              return const SizedBox();
                            },
                          ),
                        ],
                      ),
                    ),
                    DiscussionList(issue: issue),
                    const SizedBox(height: 40),
                  ],
                ),
              ),
            );
          }
          return const Center(child: CircularProgressIndicator());
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => showModalBottomSheet(
          context: context,
          showDragHandle: true,
          isScrollControlled: true,
          useSafeArea: true,
          builder: (context) => Padding(
            padding: EdgeInsets.only(
                bottom: MediaQuery.of(context).viewInsets.bottom),
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: MarkdownField(
                autofocus: true,
                onSubmit: (value) async {
                  await widget.issue!.comment(value);
                  getData();
                  if (context.mounted) Navigator.pop(context);
                },
              ),
            ),
          ),
        ),
        child: const Icon(Icons.comment),
      ),
    );
  }
}

class IssueDetailRoute extends MaterialPageRoute<void> {
  IssueDetailRoute({
    Issue? issue,
    int? iid,
    String? projectPath,
    int? projectId,
  }) : super(
          builder: (BuildContext context) => IssueDetail(
            issue: issue,
            iid: iid,
            projectPath: projectPath,
            projectId: projectId,
          ),
        );
}
