import 'dart:async';

import 'package:flutter/material.dart';
import 'package:gitlab/issues/issue_model.dart';
import 'package:gitlab/issues/widgets/detail/drawer/widgets.dart';
import 'package:gitlab/projects/project_model.dart';
import 'package:gitlab/users/widgets/list_tile.dart';

import '../../../../users/user_model.dart';
import '../../../../users/widgets/avatar.dart';

class IssueAssignees extends StatefulWidget {
  final Issue issue;
  final void Function(Issue issue)? onChanged;
  const IssueAssignees(this.issue, {this.onChanged, super.key});

  @override
  State<IssueAssignees> createState() => _IssueAssigneesState();
}

class _IssueAssigneesState extends State<IssueAssignees> {
  late Issue _issue = widget.issue;

  @override
  Widget build(BuildContext context) {
    return DrawerSection(
      title: _issue.assignees.length == 1 ? 'Assignee' : 'Assignees',
      child: _issue.assignees.isEmpty
          ? const EmptyWidget()
          : Column(
              children: _issue.assignees
                  .map(
                    (user) => Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8),
                      child: Row(
                        children: [
                          UserAvatar(
                            user: user,
                            size: 30,
                          ),
                          const SizedBox(width: 8),
                          Text(user.name),
                        ],
                      ),
                    ),
                  )
                  .toList(),
            ),
      onEdit: () => showModalBottomSheet(
        context: context,
        showDragHandle: true,
        isScrollControlled: true,
        useSafeArea: true,
        builder: (context) => Padding(
          padding:
              EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
          child: _EditIssueAssignees(
            widget.issue,
            onSave: (saved) => setState(() {
              print('changed');
              _issue = saved;
            }),
          ),
        ),
      ),
    );
  }
}

class _EditIssueAssignees extends StatefulWidget {
  final Issue issue;
  final void Function(Issue issue)? onSave;
  const _EditIssueAssignees(this.issue, {this.onSave});

  @override
  State<_EditIssueAssignees> createState() => _EditIssueAssigneesState();
}

class _EditIssueAssigneesState extends State<_EditIssueAssignees> {
  String? _query;
  final StreamController<List<User>> _userStream =
      StreamController<List<User>>();

  @override
  void initState() {
    print(widget.onSave);
    super.initState();
    getData();
  }

  void getData() async {
    var users = await Project.getUsers(
      widget.issue.projectId,
      search: _query,
      skipUsers: widget.issue.assignees.map((e) => e.id).toList(),
    );
    _userStream.add(users);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Flexible(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _currentWidget(),
                const Divider(),
                _otherUsersWidget(),
              ],
            ),
          ),
        ),
        DrawerEditSearch(
          hintText: 'Filter users',
          onChanged: (value) {
            _query = value;
            getData();
          },
        ),
      ],
    );
  }

  Widget _currentWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
          child: Text('Current assignees',
              style: Theme.of(context).textTheme.labelLarge),
        ),
        widget.issue.assignees.isEmpty
            ? const Padding(
                padding: EdgeInsets.all(16),
                child: Text(
                  'No users are assigned to this issue',
                  style: TextStyle(fontStyle: FontStyle.italic),
                ),
              )
            : ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: widget.issue.assignees.length,
                itemBuilder: (context, index) => UserListTile(
                  widget.issue.assignees[index],
                  trailing: IconButton(
                    icon: const Icon(Icons.close),
                    onPressed: () => setState(() {
                      widget.issue.assignees.removeWhere((element) =>
                          element.id == widget.issue.assignees[index].id);
                      getData();
                      _save();
                    }),
                  ),
                ),
              ),
      ],
    );
  }

  Widget _otherUsersWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
          child: Text('Other users',
              style: Theme.of(context).textTheme.labelLarge),
        ),
        StreamBuilder(
          stream: _userStream.stream,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(child: CircularProgressIndicator());
            }
            return snapshot.data!.isEmpty
                ? const Padding(
                    padding: EdgeInsets.all(16),
                    child: Text(
                      'No users match the search query',
                      style: TextStyle(fontStyle: FontStyle.italic),
                    ),
                  )
                : ListView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: snapshot.data!.length,
                    itemBuilder: (context, index) => UserListTile(
                      snapshot.data![index],
                      onTap: () => setState(() {
                        widget.issue.assignees.add(snapshot.data![index]);
                        getData();
                        _save();
                      }),
                    ),
                  );
          },
        ),
      ],
    );
  }

  void _save() async {
    var result = await widget.issue.update(
      assigneeIds: widget.issue.assignees.map((e) => e.id).toList(),
    );
    if (result != null && widget.onSave != null) widget.onSave!(result);
  }
}
