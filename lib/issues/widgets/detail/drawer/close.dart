import 'package:flutter/material.dart';
import 'package:gitlab/issues/issue_model.dart';
import 'package:gitlab/issues/widgets/detail/drawer/widgets.dart';

class IssueClose extends StatefulWidget {
  final Issue issue;
  const IssueClose(this.issue, {super.key});

  @override
  State<IssueClose> createState() => _IssueCloseState();
}

class _IssueCloseState extends State<IssueClose> {
  late bool _closed = widget.issue.state == 'closed';

  @override
  Widget build(BuildContext context) {
    return DrawerAction(
      label: '${_closed ? 'Reopen' : 'Close'} issue',
      leading: const Icon(Icons.assignment_turned_in),
      onTap: () async {
        setState(() {
          _closed = !_closed;
        });
        await widget.issue.update(stateEvent: _closed ? 'close' : 'reopen');
      },
    );
  }
}
