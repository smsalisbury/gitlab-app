import 'package:flutter/material.dart';
import 'package:gitlab/issues/widgets/detail/drawer/widgets.dart';

import '../../../issue_model.dart';

class IssueConfidentiality extends StatefulWidget {
  final Issue issue;

  const IssueConfidentiality(this.issue, {super.key});

  @override
  State<IssueConfidentiality> createState() => _IssueConfidentialityState();
}

class _IssueConfidentialityState extends State<IssueConfidentiality> {
  late bool _confidential = widget.issue.confidential;

  @override
  Widget build(BuildContext context) {
    return DrawerSection(
      title: 'Confidentiality',
      child: _confidential
          ? Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Row(
                  children: [
                    Icon(
                      Icons.visibility_off,
                      color: Theme.of(context).primaryColor,
                    ),
                    const SizedBox(width: 8),
                    const Text('Confidential'),
                  ],
                ),
                const SizedBox(height: 8),
                Card(
                  elevation: 0,
                  color: Theme.of(context).colorScheme.surfaceVariant,
                  child: Padding(
                    padding: const EdgeInsets.all(16),
                    child: Text(
                        'Only project members with at least the Reporter role, the author, and assignees can view or be notified about this issue.',
                        style: TextStyle(
                            color: Theme.of(context)
                                .colorScheme
                                .onSurfaceVariant)),
                  ),
                )
              ],
            )
          : const Row(
              children: [
                Icon(Icons.visibility),
                SizedBox(width: 8),
                Text('Not confidential'),
              ],
            ),
      onEdit: () async {
        setState(() {
          _confidential = !_confidential;
        });
        await widget.issue.update(confidential: _confidential);
      },
    );
  }
}
