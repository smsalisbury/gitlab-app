import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gitlab/issues/issue_model.dart';
import 'package:gitlab/issues/widgets/detail/drawer/widgets.dart';

class IssueCopyReference extends StatelessWidget {
  final Issue issue;
  const IssueCopyReference(this.issue, {super.key});

  @override
  Widget build(BuildContext context) {
    return DrawerAction(
      label: 'Copy reference',
      leading: const Icon(Icons.copy),
      onTap: () {
        Clipboard.setData(ClipboardData(text: issue.references.full));
        ScaffoldMessenger.of(context)
            .showSnackBar(const SnackBar(content: Text('Reference copied')));
      },
    );
  }
}
