import 'package:flutter/material.dart';
import 'package:gitlab/issues/widgets/detail/drawer/assignees.dart';
import 'package:gitlab/issues/widgets/detail/drawer/close.dart';
import 'package:gitlab/issues/widgets/detail/drawer/confidentiality.dart';
import 'package:gitlab/issues/widgets/detail/drawer/copy_ref.dart';
import 'package:gitlab/issues/widgets/detail/drawer/edit.dart';
import 'package:gitlab/issues/widgets/detail/drawer/epic.dart';
import 'package:gitlab/issues/widgets/detail/drawer/labels.dart';
import 'package:gitlab/issues/widgets/detail/drawer/lock.dart';
import 'package:gitlab/issues/widgets/detail/drawer/milestone.dart';
import 'package:gitlab/issues/widgets/detail/drawer/notifications.dart';
import 'package:gitlab/issues/widgets/detail/drawer/participants.dart';
import 'package:gitlab/issues/widgets/detail/drawer/weight.dart';
import 'package:gitlab/issues/widgets/detail/drawer/widgets.dart';

import '../../../issue_model.dart';

class IssueDrawer extends StatelessWidget {
  final Issue issue;
  final void Function()? onChanged;

  const IssueDrawer(this.issue, {super.key, this.onChanged});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: SafeArea(
        child: SingleChildScrollView(
          child: Column(children: [
            IssueAssignees(issue),
            if (issue.features?.epic == true) IssueEpic(issue),
            IssueLabels(issue),
            IssueMilestone(issue),
            if (issue.features?.weight == true) IssueWeight(issue),
            IssueConfidentiality(issue),
            IssueParticipants(issue),
            const Divider(),
            IssueNotifications(issue),
            const DrawerAction(
              label: 'New related issue',
              leading: Icon(Icons.control_point_duplicate),
              implemented: false,
            ),
            if (issue.features?.epic == true)
              const DrawerAction(
                label: 'Promote to epic',
                leading: Icon(Icons.account_tree),
                implemented: false,
              ),
            IssueCopyReference(issue),
            const DrawerAction(
              label: 'Copy issue email address',
              leading: Icon(Icons.email),
              implemented: false,
            ),
            const Divider(),
            IssueEdit(
              issue,
              onSaved: (value) => {
                if (onChanged != null) onChanged!(),
              },
            ),
            IssueDiscussionLock(issue),
            IssueClose(issue),
          ]),
        ),
      ),
    );
  }
}
