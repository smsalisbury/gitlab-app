import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:gitlab/issues/widgets/detail/drawer/widgets.dart';
import 'package:gitlab/widgets/markdown/markdown_input.dart';

import '../../../issue_model.dart';

class IssueEdit extends StatelessWidget {
  final Issue issue;
  final void Function(Issue? value)? onSaved;

  const IssueEdit(this.issue, {super.key, this.onSaved});

  @override
  Widget build(BuildContext context) {
    return DrawerAction(
      label: 'Edit issue',
      leading: const Icon(Icons.edit),
      onTap: () => showModalBottomSheet(
        context: context,
        showDragHandle: true,
        isScrollControlled: true,
        useSafeArea: true,
        builder: (context) => Padding(
          padding:
              EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
          child: _EditIssueForm(
            issue,
            onSaved: onSaved,
          ),
        ),
      ),
    );
  }
}

class _EditIssueForm extends StatefulWidget {
  final Issue issue;
  final void Function(Issue? value)? onSaved;

  const _EditIssueForm(this.issue, {this.onSaved});

  @override
  State<_EditIssueForm> createState() => _EditIssueFormState();
}

class _EditIssueFormState extends State<_EditIssueForm> {
  final _formKey = GlobalKey<FormState>();
  final formData = HashMap<String, String>();
  bool _saving = false;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Flex(
          direction: Axis.vertical,
          children: [
            TextFormField(
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                label: Text('Title'),
              ),
              initialValue: widget.issue.title,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Title is required';
                }
                return null;
              },
              onSaved: (newValue) => formData['title'] = newValue ?? '',
            ),
            const SizedBox(height: 16),
            Expanded(
              child: MarkdownField(
                initialValue: widget.issue.description,
                expands: true,
                decoration: const InputDecoration(labelText: 'Description'),
                onSaved: (newValue) => formData['description'] = newValue ?? '',
                showSend: false,
              ),
            ),
            const SizedBox(height: 8),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                FilledButton(
                  onPressed: _saving
                      ? null
                      : () async {
                          if (_formKey.currentState!.validate()) {
                            setState(() {
                              _saving = true;
                            });
                            _formKey.currentState!.save();
                            var result = await widget.issue.update(
                              title: formData['title'],
                              description: formData['description'],
                            );
                            if (widget.onSaved != null) {
                              widget.onSaved!(result);
                            }
                            setState(() {
                              _saving = false;
                            });
                            if (context.mounted) Navigator.pop(context);
                          }
                        },
                  child: _saving
                      ? Transform.scale(
                          scale: .5,
                          child: const CircularProgressIndicator(),
                        )
                      : const Text('Save'),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
