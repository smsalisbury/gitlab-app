import 'dart:async';

import 'package:flutter/material.dart';
import 'package:gitlab/issues/epic_model.dart';
import 'package:gitlab/issues/widgets/detail/drawer/widgets.dart';

import '../../../../projects/project_model.dart';
import '../../../issue_model.dart';

class IssueEpic extends StatefulWidget {
  final Issue issue;

  const IssueEpic(this.issue, {super.key});

  @override
  State<IssueEpic> createState() => _IssueEpicState();
}

class _IssueEpicState extends State<IssueEpic> {
  late Epic? _epic = widget.issue.epic;

  @override
  Widget build(BuildContext context) {
    return DrawerSection(
      title: 'Epic',
      child: _epic == null ? null : Text(_epic!.title),
      onEdit: () => showModalBottomSheet(
        context: context,
        showDragHandle: true,
        isScrollControlled: true,
        useSafeArea: true,
        builder: (context) => Padding(
          padding:
              EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
          child: _EditIssueEpic(
            widget.issue,
            onSave: (epic) => setState(() {
              _epic = epic;
            }),
          ),
        ),
      ),
    );
  }
}

class _EditIssueEpic extends StatefulWidget {
  final Issue issue;
  final void Function(Epic? epic)? onSave;
  const _EditIssueEpic(this.issue, {this.onSave});

  @override
  State<_EditIssueEpic> createState() => _EditIssueEpicState();
}

class _EditIssueEpicState extends State<_EditIssueEpic> {
  String? _query;
  final StreamController<List<Epic>> _epicStream =
      StreamController<List<Epic>>();
  late Epic? _currentEpic = widget.issue.epic;

  @override
  void initState() {
    super.initState();
    getData();
  }

  void getData() async {
    var project = await Project.get(id: widget.issue.projectId, cached: false);
    if (project?.namespace?.kind != 'group') {
      return;
    }
    var epics = await Epic.listForGroup(
      project!.namespace!.id,
      search: _query,
    );
    _epicStream.add(epics.data);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Flexible(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _currentWidget(),
                const Divider(),
                _otherItemsWidget(),
              ],
            ),
          ),
        ),
        DrawerEditSearch(
          hintText: 'Filter epics',
          onChanged: (value) {
            _query = value;
            getData();
          },
        )
      ],
    );
  }

  Widget _currentWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
          child: Text('Current epic',
              style: Theme.of(context).textTheme.labelLarge),
        ),
        _currentEpic == null
            ? const Padding(
                padding: EdgeInsets.all(16),
                child: Text(
                  'No epic currently assigned to this issue',
                  style: TextStyle(fontStyle: FontStyle.italic),
                ),
              )
            : ListTile(
                title: Text(_currentEpic!.title),
                trailing: IconButton(
                  icon: const Icon(Icons.close),
                  onPressed: () => setState(() {
                    _currentEpic!.removeIssue(widget.issue);
                    _currentEpic = null;
                    _onSave();
                  }),
                ),
              ), // TODO: remove icon
      ],
    );
  }

  Widget _otherItemsWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
          child: Text('Other epics',
              style: Theme.of(context).textTheme.labelLarge),
        ),
        StreamBuilder(
          stream: _epicStream.stream,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(child: CircularProgressIndicator());
            }
            var displayEpics = snapshot.data
                ?.where((element) => element.id != _currentEpic?.id)
                .toList();
            return displayEpics == null || displayEpics.isEmpty
                ? const Padding(
                    padding: EdgeInsets.all(16),
                    child: Text(
                      'No epics match the search query',
                      style: TextStyle(fontStyle: FontStyle.italic),
                    ),
                  )
                : ListView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: displayEpics.length,
                    itemBuilder: (context, index) => ListTile(
                      title: Text(displayEpics[index].title),
                      onTap: () => setState(() {
                        _currentEpic = displayEpics[index];
                        _currentEpic!.addIssue(widget.issue);
                        _onSave();
                      }),
                    ),
                  );
          },
        ),
      ],
    );
  }

  void _onSave() {
    if (widget.onSave != null) widget.onSave!(_currentEpic);
  }
}
