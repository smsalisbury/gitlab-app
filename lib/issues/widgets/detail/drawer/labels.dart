import 'dart:async';

import 'package:flutter/material.dart';
import 'package:gitlab/issues/widgets/detail/drawer/widgets.dart';
import 'package:gitlab/widgets/labels/label.dart';
import 'package:gitlab/widgets/labels/label_model.dart';

import '../../../issue_model.dart';

class IssueLabels extends StatefulWidget {
  final Issue issue;

  const IssueLabels(this.issue, {super.key});

  @override
  State<IssueLabels> createState() => _IssueLabelsState();
}

class _IssueLabelsState extends State<IssueLabels> {
  late List<String> _labels = widget.issue.labels;

  @override
  Widget build(BuildContext context) {
    return DrawerSection(
      title: 'Labels',
      child: widget.issue.labels.isEmpty
          ? null
          : Wrap(
              spacing: 4,
              runSpacing: 4,
              children: _labels
                  .map((label) => Label(
                        label,
                        projectId: widget.issue.projectId,
                        key: UniqueKey(),
                      ))
                  .toList(),
            ),
      onEdit: () => showModalBottomSheet(
        context: context,
        showDragHandle: true,
        isScrollControlled: true,
        useSafeArea: true,
        builder: (context) => Padding(
          padding:
              EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
          child: _EditIssueLabels(
            widget.issue,
            onSave: (labels) => setState(() {
              _labels = labels;
            }),
          ),
        ),
      ),
    );
  }
}

class _EditIssueLabels extends StatefulWidget {
  final Issue issue;
  final void Function(List<String> labels)? onSave;

  const _EditIssueLabels(this.issue, {this.onSave});

  @override
  State<_EditIssueLabels> createState() => _EditIssueLabelsState();
}

class _EditIssueLabelsState extends State<_EditIssueLabels> {
  late final List<String> _currentLabels = widget.issue.labels;
  String? _query;
  final StreamController<List<LabelModel>> _labelStream =
      StreamController<List<LabelModel>>();

  @override
  void initState() {
    super.initState();
    getData();
  }

  void getData() async {
    var labels = await LabelModel.listForProject(
      widget.issue.projectId,
      search: _query,
    );
    _labelStream.add(labels.data);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Flexible(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _currentWidget(),
                const Divider(),
                _otherItemsWidget(),
              ],
            ),
          ),
        ),
        DrawerEditSearch(
          hintText: 'Filter labels',
          onChanged: (value) {
            _query = value;
            getData();
          },
        )
      ],
    );
  }

  Widget _currentWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
          child: Text('Current labels',
              style: Theme.of(context).textTheme.labelLarge),
        ),
        _currentLabels.isEmpty
            ? const Padding(
                padding: EdgeInsets.all(16),
                child: Text(
                  'No labels currently assigned to this issue',
                  style: TextStyle(fontStyle: FontStyle.italic),
                ),
              )
            : ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: _currentLabels.length,
                itemBuilder: (context, index) => ListTile(
                  title: Row(
                    children: [
                      Label(
                        _currentLabels[index],
                        projectId: widget.issue.projectId,
                        key: UniqueKey(),
                      ),
                    ],
                  ),
                  trailing: IconButton(
                    icon: const Icon(Icons.close),
                    onPressed: () => setState(() {
                      _currentLabels.removeWhere(
                          (element) => element == _currentLabels[index]);
                      _save();
                    }),
                  ),
                ),
              ),
      ],
    );
  }

  Widget _otherItemsWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
          child: Text(
            'Other labels',
            style: Theme.of(context).textTheme.labelLarge,
          ),
        ),
        StreamBuilder(
          stream: _labelStream.stream,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(child: CircularProgressIndicator());
            }
            var displayLabels = snapshot.data
                ?.where((element) => !_currentLabels.contains(element.name))
                .toList();
            return displayLabels == null || displayLabels.isEmpty
                ? const Padding(
                    padding: EdgeInsets.all(16),
                    child: Text(
                      'No labels match the search query',
                      style: TextStyle(fontStyle: FontStyle.italic),
                    ),
                  )
                : ListView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: displayLabels.length,
                    itemBuilder: (context, index) => ListTile(
                      dense: true,
                      title: Row(
                        children: [
                          LabelFromModel(displayLabels[index]),
                        ],
                      ),
                      onTap: () => setState(() {
                        _currentLabels.add(displayLabels[index].name);
                        _save();
                      }),
                    ),
                  );
          },
        ),
      ],
    );
  }

  void _save() async {
    await widget.issue.update(labels: _currentLabels);
    if (widget.onSave != null) widget.onSave!(_currentLabels);
  }
}
