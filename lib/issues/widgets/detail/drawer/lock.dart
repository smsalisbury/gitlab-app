import 'package:flutter/material.dart';
import 'package:gitlab/issues/issue_model.dart';
import 'package:gitlab/issues/widgets/detail/drawer/widgets.dart';

class IssueDiscussionLock extends StatefulWidget {
  final Issue issue;
  const IssueDiscussionLock(this.issue, {super.key});

  @override
  State<IssueDiscussionLock> createState() => _IssueDiscussionLockState();
}

class _IssueDiscussionLockState extends State<IssueDiscussionLock> {
  late bool _locked = widget.issue.discussionLocked == true;

  @override
  Widget build(BuildContext context) {
    return DrawerAction(
      label: '${_locked ? 'Unlock' : 'Lock'} issue',
      leading: Icon(_locked ? Icons.lock_outline : Icons.lock_open),
      onTap: () async {
        setState(() {
          _locked = !_locked;
        });
        await widget.issue.update(discussionLocked: _locked);
      },
    );
  }
}
