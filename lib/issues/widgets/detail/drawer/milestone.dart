import 'dart:async';

import 'package:flutter/material.dart';
import 'package:gitlab/issues/widgets/detail/drawer/widgets.dart';
import 'package:gitlab/planning/milestones/milestone_model.dart';

import '../../../issue_model.dart';

class IssueMilestone extends StatefulWidget {
  final Issue issue;
  const IssueMilestone(this.issue, {super.key});

  @override
  State<IssueMilestone> createState() => _IssueMilestoneState();
}

class _IssueMilestoneState extends State<IssueMilestone> {
  late Milestone? _milestone = widget.issue.milestone;

  @override
  Widget build(BuildContext context) {
    return DrawerSection(
      title: 'Milestone',
      child: _milestone == null ? null : Text(_milestone!.title),
      onEdit: () => showModalBottomSheet(
        context: context,
        showDragHandle: true,
        isScrollControlled: true,
        useSafeArea: true,
        builder: (context) => Padding(
          padding:
              EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
          child: _EditIssueMilestone(
            widget.issue,
            onSave: (milestone) => setState(() {
              _milestone = milestone;
            }),
          ),
        ),
      ),
    );
  }
}

class _EditIssueMilestone extends StatefulWidget {
  final Issue issue;
  final void Function(Milestone? milestone)? onSave;
  const _EditIssueMilestone(this.issue, {this.onSave});

  @override
  State<_EditIssueMilestone> createState() => __EditIssueMilestoneState();
}

class __EditIssueMilestoneState extends State<_EditIssueMilestone> {
  String? _query;
  final StreamController<List<Milestone>> _milestoneStream =
      StreamController<List<Milestone>>();
  late Milestone? _currentMilestone = widget.issue.milestone;

  @override
  void initState() {
    super.initState();
    getData();
  }

  void getData() async {
    var milestones = await Milestone.listForProject(
      widget.issue.projectId,
      search: _query,
      includeParentMilestones: true,
      state: 'active',
    );
    _milestoneStream.add(milestones);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Flexible(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _currentWidget(),
                const Divider(),
                _otherItemsWidget(),
              ],
            ),
          ),
        ),
        DrawerEditSearch(
          hintText: 'Filter milestones',
          onChanged: (value) {
            _query = value;
            getData();
          },
        )
      ],
    );
  }

  Widget _currentWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
          child: Text('Current milestone',
              style: Theme.of(context).textTheme.labelLarge),
        ),
        _currentMilestone == null
            ? const Padding(
                padding: EdgeInsets.all(16),
                child: Text(
                  'No milestone currently assigned to this issue',
                  style: TextStyle(fontStyle: FontStyle.italic),
                ),
              )
            : ListTile(
                title: Text(_currentMilestone!.title),
                trailing: IconButton(
                  icon: const Icon(Icons.close),
                  onPressed: () async {
                    setState(() {
                      _currentMilestone = null;
                    });
                    await widget.issue.update(milestoneId: 0);
                    if (widget.onSave != null) widget.onSave!(null);
                  },
                ),
              ),
      ],
    );
  }

  Widget _otherItemsWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
          child: Text('Other milestones',
              style: Theme.of(context).textTheme.labelLarge),
        ),
        StreamBuilder(
          stream: _milestoneStream.stream,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(child: CircularProgressIndicator());
            }
            var displayMilestones = snapshot.data
                ?.where((element) => element.id != _currentMilestone?.id)
                .toList();
            return displayMilestones == null || displayMilestones.isEmpty
                ? const Padding(
                    padding: EdgeInsets.all(16),
                    child: Text(
                      'No milestones match the search query',
                      style: TextStyle(fontStyle: FontStyle.italic),
                    ),
                  )
                : ListView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: displayMilestones.length,
                    itemBuilder: (context, index) => ListTile(
                      title: Text(displayMilestones[index].title),
                      onTap: () async {
                        setState(() {
                          _currentMilestone = displayMilestones[index];
                        });
                        await widget.issue
                            .update(milestoneId: displayMilestones[index].id);
                        if (widget.onSave != null) {
                          widget.onSave!(_currentMilestone);
                        }
                      },
                    ),
                  );
          },
        ),
      ],
    );
  }
}
