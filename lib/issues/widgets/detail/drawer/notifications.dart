import 'package:flutter/material.dart';
import 'package:gitlab/issues/issue_model.dart';
import 'package:gitlab/issues/widgets/detail/drawer/widgets.dart';

class IssueNotifications extends StatefulWidget {
  final Issue issue;
  const IssueNotifications(this.issue, {super.key});

  @override
  State<IssueNotifications> createState() => _IssueNotificationsState();
}

class _IssueNotificationsState extends State<IssueNotifications> {
  late bool _subscribed = widget.issue.subscribed == true;

  @override
  Widget build(BuildContext context) {
    return DrawerAction(
      label: 'Notifications',
      leading:
          Icon(_subscribed ? Icons.notifications : Icons.notifications_off),
      onTap: () => _toggle(),
      trailing: Switch(
        value: _subscribed,
        onChanged: (val) => _toggle(),
      ),
    );
  }

  Future<void> _toggle() async {
    setState(() {
      _subscribed = !_subscribed;
    });
    if (_subscribed) {
      await widget.issue.unsubscribe();
    } else {
      await widget.issue.subscribe();
    }
  }
}
