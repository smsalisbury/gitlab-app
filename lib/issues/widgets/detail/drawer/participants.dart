import 'package:flutter/material.dart';
import 'package:gitlab/issues/widgets/detail/drawer/widgets.dart';
import 'package:gitlab/users/user_model.dart';
import 'package:gitlab/users/widgets/avatar.dart';
import 'package:gitlab/widgets/loader/shapes.dart';

import '../../../issue_model.dart';

class IssueParticipants extends StatefulWidget {
  final Issue issue;
  const IssueParticipants(this.issue, {super.key});

  @override
  State<IssueParticipants> createState() => _IssueParticipantsState();
}

class _IssueParticipantsState extends State<IssueParticipants> {
  late final Future<List<User>> _participants = widget.issue.participants();

  @override
  Widget build(BuildContext context) {
    return DrawerSection(
      title: 'Participants',
      child: FutureBuilder(
        future: _participants,
        builder: ((context, snapshot) {
          if (snapshot.hasData) {
            return Column(
              children: snapshot.data!.map((e) => _UserWidget(e)).toList(),
            );
          }
          return const _EmptyWidget();
        }),
      ),
    );
  }
}

class _EmptyWidget extends StatelessWidget {
  const _EmptyWidget();

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.symmetric(vertical: 8.0),
      child: Row(
        children: [
          SkeletonAvatar(radius: 15),
          SizedBox(width: 8),
          Expanded(child: SkeletonLine()),
        ],
      ),
    );
  }
}

class _UserWidget extends StatelessWidget {
  final User user;
  const _UserWidget(this.user);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Row(
        children: [
          UserAvatar(
            user: user,
            size: 30,
          ),
          const SizedBox(width: 8),
          Text(user.name),
        ],
      ),
    );
  }
}
