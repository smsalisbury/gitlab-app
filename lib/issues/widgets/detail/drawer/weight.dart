import 'package:flutter/material.dart';
import 'package:gitlab/issues/widgets/detail/drawer/widgets.dart';

import '../../../issue_model.dart';

class IssueWeight extends StatefulWidget {
  final Issue issue;

  const IssueWeight(this.issue, {super.key});

  @override
  State<IssueWeight> createState() => _IssueWeightState();
}

class _IssueWeightState extends State<IssueWeight> {
  late int? _weight = widget.issue.weight;

  @override
  Widget build(BuildContext context) {
    return DrawerSection(
      title: 'Weight',
      child: _weight == null ? null : Text(_weight.toString()),
      onEdit: () => showModalBottomSheet(
        context: context,
        showDragHandle: true,
        isScrollControlled: true,
        useSafeArea: true,
        builder: (context) => Padding(
          padding:
              EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
          child: _EditIssueWeight(
            widget.issue,
            onSave: (weight) => setState(() {
              _weight = weight;
            }),
          ),
        ),
      ),
    );
  }
}

class _EditIssueWeight extends StatefulWidget {
  final Issue issue;
  final void Function(int? weight)? onSave;
  const _EditIssueWeight(this.issue, {this.onSave});

  @override
  State<_EditIssueWeight> createState() => _EditIssueWeightState();
}

class _EditIssueWeightState extends State<_EditIssueWeight> {
  final TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    _controller.text = widget.issue.weight.toString();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: const EdgeInsets.all(16),
          child: TextField(
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'Weight',
            ),
            controller: _controller,
            onChanged: (val) async {
              var intVal = val == '' ? null : int.parse(val);
              if (widget.onSave != null) {
                widget.onSave!(intVal);
              }
              if (intVal == null) {
                await widget.issue.removeWeight();
              } else {
                await widget.issue.update(weight: intVal);
              }
            },
          ),
        )
      ],
    );
  }
}
