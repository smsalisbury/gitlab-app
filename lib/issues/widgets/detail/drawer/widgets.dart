import 'package:flutter/material.dart';

class EmptyWidget extends StatelessWidget {
  final String text;
  const EmptyWidget({this.text = 'None', super.key});

  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: 0.5,
      child: Text(text),
    );
  }
}

class DrawerSection extends StatelessWidget {
  final String? title;
  final Widget? child;
  final void Function()? onEdit;

  const DrawerSection({
    this.child,
    this.title,
    this.onEdit,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: title == null
          ? null
          : Text(title!, style: Theme.of(context).textTheme.labelLarge),
      subtitle: child ?? const EmptyWidget(),
      onTap: onEdit,
    );
  }
}

class DrawerEditSearch extends StatelessWidget {
  final String? hintText;
  final void Function(String value)? onChanged;

  const DrawerEditSearch({super.key, this.hintText, this.onChanged});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: TextField(
        decoration: InputDecoration(
          border: const OutlineInputBorder(),
          hintText: hintText,
        ),
        onChanged: onChanged,
      ),
    );
  }
}

class DrawerAction extends StatelessWidget {
  final String label;
  final Widget? leading;
  final Widget? trailing;
  final void Function()? onTap;
  final bool? implemented;

  const DrawerAction({
    super.key,
    required this.label,
    this.leading,
    this.trailing,
    this.onTap,
    this.implemented,
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: leading,
      trailing: trailing,
      title: Text(label, style: Theme.of(context).textTheme.labelLarge),
      subtitle: implemented == false
          ? Text(
              'Coming soon!'.toUpperCase(),
              style: Theme.of(context).textTheme.bodySmall?.copyWith(
                    color: Theme.of(context).colorScheme.error,
                  ),
            )
          : null,
      dense: true,
      onTap: onTap,
    );
  }
}
