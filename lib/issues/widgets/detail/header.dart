import 'package:flutter/material.dart';
import 'package:gitlab/issues/widgets/detail/state.dart';
import 'package:timeago/timeago.dart' as timeago;

import '../../../common/text.dart';
import '../../../users/widgets/avatar.dart';
import '../../issue_model.dart';

class IssueDetailHeader extends StatelessWidget {
  final Issue issue;
  const IssueDetailHeader(this.issue, {super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TitleText(text: issue.title),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              IssueState(issue: issue, size: 12),
              const SizedBox(width: 6),
              SubtitleText(
                text: 'Created ${timeago.format(issue.createdAt)} ago by',
              ),
              const SizedBox(width: 4),
              UserAvatar(user: issue.author, size: 20),
              const SizedBox(width: 4),
              SubtitleText(text: issue.author.name),
            ],
          ),
        ),
      ],
    );
  }
}
