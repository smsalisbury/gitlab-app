import 'package:flutter/material.dart';

import '../../issue_model.dart';

class IssueState extends StatelessWidget {
  final Issue issue;
  final double? size;

  const IssueState({super.key, required this.issue, this.size});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Chip(
          avatar: Icon(icon, color: fgColor, size: size),
          backgroundColor: bgColor,
          label: Text(text, style: TextStyle(color: fgColor, fontSize: size)),
          side: BorderSide.none,
          visualDensity: VisualDensity.compact,
        ),
        ...(issue.confidential
            ? [
                const SizedBox(width: 8),
                Chip(
                  avatar: Icon(Icons.visibility_off,
                      color: Theme.of(context).colorScheme.onSurfaceVariant,
                      size: size),
                  backgroundColor: Theme.of(context).colorScheme.surfaceVariant,
                  label: Text('Confidential',
                      style: TextStyle(
                          color: Theme.of(context).colorScheme.onSurfaceVariant,
                          fontSize: size)),
                  side: BorderSide.none,
                  visualDensity: VisualDensity.compact,
                ),
              ]
            : []),
      ],
    );
  }

  String get text {
    Map<String, String> stateMap = Map.from({
      'opened': 'Open',
      'closed': 'Closed',
    });
    return stateMap[issue.state] ?? issue.state;
  }

  Color get fgColor {
    Map<String, Color> stateMap = Map.from({
      'opened': Colors.green.shade600,
    });
    return stateMap[issue.state] ?? Colors.black;
  }

  Color get bgColor {
    Map<String, Color> stateMap = Map.from({
      'opened': Colors.green.shade100,
    });
    return stateMap[issue.state] ?? Colors.grey;
  }

  IconData get icon {
    Map<String, IconData> stateMap = Map.from({
      'opened': Icons.web_stories,
    });
    return stateMap[issue.state] ?? Icons.abc;
  }
}
