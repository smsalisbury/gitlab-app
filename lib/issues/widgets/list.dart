import 'package:flutter/material.dart';
import 'package:gitlab/api_service.dart';
import 'package:gitlab/widgets/http-response.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import '../../issues/issue_model.dart';
import '../../issues/widgets/list_tile.dart';
import '../../widgets/loader/list.dart';

class IssueListWidget extends StatefulWidget {
  const IssueListWidget({super.key});

  @override
  State<IssueListWidget> createState() => IssueListWidgetState();
}

class IssueListWidgetState<T extends IssueListWidget> extends State<T> {
  final pageSize = 20;
  final PagingController<int, Issue> pagingController =
      PagingController(firstPageKey: 0);

  @override
  void initState() {
    super.initState();
    pagingController.addPageRequestListener((pageKey) => fetchPage(pageKey));
  }

  Future<void> fetchPage(int pageKey) async {
    try {
      var page = (pageKey / pageSize).floor() + 1;
      var list = await getIssues(page, pageSize);
      print(
          'fetch page | key: $pageKey, page: $page, length: ${list.data.length}, total: ${list.total}');
      final isLastPage = pageKey + list.data.length == list.total;
      if (isLastPage) {
        pagingController.appendLastPage(list.data);
      } else {
        final nextPageKey = pageKey + list.data.length;
        pagingController.appendPage(list.data, nextPageKey);
      }
    } catch (error) {
      pagingController.error = error;
    }
  }

  Future<PaginatedAPIList<Issue>> getIssues(int page, int size) async {
    return Issue.list(
      page: page,
      perPage: pageSize,
    );
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () => Future.sync(() => pagingController.refresh()),
      child: PagedListView(
        pagingController: pagingController,
        builderDelegate: PagedChildBuilderDelegate<Issue>(
          itemBuilder: (context, item, index) => IssueListTile(issue: item),
          firstPageProgressIndicatorBuilder: (context) => const SizedBox(
            width: double.infinity,
            height: 0,
            child: SkeletonListView(),
          ),
          firstPageErrorIndicatorBuilder: (context) => _ErrorPage(
              onTryAgain: () => pagingController.refresh(),
              child: HttpResponse(pagingController.error)),
          newPageProgressIndicatorBuilder: (context) => const SizedBox(
            width: double.infinity,
            height: 72,
            child: SkeletonListView(itemCount: 1),
          ),
        ),
      ),
    );
  }
}

class _ErrorPage extends StatelessWidget {
  final void Function()? onTryAgain;
  final Widget? child;

  const _ErrorPage({this.onTryAgain, this.child});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 36, horizontal: 12),
      child: Column(
        children: [
          Text('GitLab sent back an error',
              style: Theme.of(context).textTheme.headlineMedium),
          const SizedBox(height: 24),
          FilledButton.icon(
            onPressed: onTryAgain,
            icon: const Icon(Icons.refresh),
            label: const Text('Try again'),
          ),
          if (child != null) const SizedBox(height: 24),
          if (child != null)
            ExpansionTile(
              title: const Text('Click here to see details'),
              children: [child!],
            ),
        ],
      ),
    );
  }
}
