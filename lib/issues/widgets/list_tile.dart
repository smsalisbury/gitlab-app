import 'package:flutter/material.dart';
import 'package:gitlab/issues/widgets/detail/detail.dart';
import 'package:gitlab/users/widgets/avatar.dart';

import '../../api_service.dart';
import '../../projects/project_model.dart';
import '../issue_model.dart';

class IssueListTile extends StatefulWidget {
  final Issue issue;

  const IssueListTile({super.key, required this.issue});

  @override
  State<IssueListTile> createState() => _IssueListTileState();
}

class _IssueListTileState extends State<IssueListTile> {
  late Project? project;

  @override
  void initState() {
    super.initState();
    project = null;
    getData();
  }

  void getData() async {
    project = (await ApiService().getProject(widget.issue.projectId));
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: UserAvatar(user: widget.issue.author),
      title: Text(
        widget.issue.title,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      ),
      subtitle: Row(
        children: [
          if (project != null) Text(project!.name),
          if (widget.issue.milestone != null) ...[
            const SizedBox(width: 8),
            Row(children: [
              const Icon(Icons.timer, size: 16),
              const SizedBox(width: 2),
              Text(widget.issue.milestone!.title)
            ])
          ],
          if (widget.issue.weight != null) ...[
            const SizedBox(width: 8),
            Row(children: [
              const Icon(Icons.fitness_center, size: 16),
              const SizedBox(width: 2),
              Text(widget.issue.weight.toString())
            ]),
          ],
        ],
      ),
      onTap: () {
        Navigator.push(context, IssueDetailRoute(issue: widget.issue));
      },
    );
  }
}
