import 'dart:convert';
import 'dart:developer';

import 'package:gitlab/common/models.dart';
import 'package:gitlab/pipelines/pipeline_model.dart';
import 'package:gitlab/planning/milestones/milestone_model.dart';
import 'package:gitlab/users/user_model.dart';
import 'package:http/http.dart' as http;

import '../api_service.dart';
import '../auth_service.dart';
import '../commit/commit_model.dart';
import '../constants.dart';

MergeRequestApprovalStatus mergeRequestApprovalsFromJson(String str) =>
    MergeRequestApprovalStatus.fromJson(json.decode(str));
MergeRequest mergeRequestFromJson(String str) =>
    MergeRequest.fromJson(json.decode(str));
List<MergeRequest> mergeRequestsFromJson(String str) => List<MergeRequest>.from(
    json.decode(str).map((x) => MergeRequest.fromJson(x)));
List<MergeRequestApprovalRule> mergeRequestApprovalRulesFromJson(String str) =>
    List<MergeRequestApprovalRule>.from(
        json.decode(str).map((x) => MergeRequestApprovalRule.fromJson(x)));

class _Urls {
  static String approvalRuleStatus =
      '${ApiConstants.versionPath}/projects/:project/merge_requests/:mr/approval_state';
  static String approvals =
      '${ApiConstants.versionPath}/projects/:project/merge_requests/:mr/approvals';
  static String approve =
      '${ApiConstants.versionPath}/projects/:project/merge_requests/:mr/approve';
  static String commits =
      '${ApiConstants.versionPath}/projects/:project/merge_requests/:mr/commits';
  static String merge =
      '${ApiConstants.versionPath}/projects/:project/merge_requests/:mr/merge';
  static String unapprove =
      '${ApiConstants.versionPath}/projects/:project/merge_requests/:mr/unapprove';
}

abstract class _BaseMergeRequest {
  final int id;
  final int iid;
  final int projectId;
  final String title;
  final String description;
  final String state;
  final DateTime createdAt;
  final DateTime updatedAt;
  final MergeStatus detailedMergeStatus;

  _BaseMergeRequest({
    required this.id,
    required this.iid,
    required this.projectId,
    required this.title,
    required this.description,
    required this.state,
    required this.createdAt,
    required this.updatedAt,
    required this.detailedMergeStatus,
  });
}

class MergeRequest extends _BaseMergeRequest {
  final User author;
  final References references;
  final String targetBranch;
  final String sourceBranch;
  final Milestone? milestone;
  final bool? shouldRemoveSourceBranch;
  final bool? forceRemoveSourceBranch;
  final bool? squashOnMerge;
  final User? mergeUser;
  final DateTime? mergedAt;

  MergeRequest({
    id,
    iid,
    projectId,
    title,
    description,
    state,
    createdAt,
    updatedAt,
    detailedMergeStatus,
    required this.author,
    required this.references,
    required this.targetBranch,
    required this.sourceBranch,
    this.milestone,
    this.shouldRemoveSourceBranch,
    this.forceRemoveSourceBranch,
    this.squashOnMerge,
    this.mergeUser,
    this.mergedAt,
  }) : super(
          id: id,
          iid: iid,
          projectId: projectId,
          title: title,
          description: description,
          state: state,
          createdAt: createdAt,
          updatedAt: updatedAt,
          detailedMergeStatus: detailedMergeStatus,
        );

  factory MergeRequest.fromJson(Map<String, dynamic> json) => MergeRequest(
        id: json["id"],
        iid: json["iid"],
        title: json["title"],
        description: json["description"],
        projectId: json["project_id"],
        state: json['state'],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        detailedMergeStatus:
            MergeStatus.fromString(json["detailed_merge_status"]),
        author: User.fromJson(json['author']),
        references: References.fromJson(json['references']),
        targetBranch: json["target_branch"],
        sourceBranch: json["source_branch"],
        milestone: json["milestone"] != null
            ? Milestone.fromJson(json["milestone"])
            : null,
        shouldRemoveSourceBranch: json["should_remove_source_branch"],
        forceRemoveSourceBranch: json["force_remove_source_branch"],
        squashOnMerge: json["squash_on_merge"],
        mergeUser: json["merge_user"] == null
            ? null
            : User.fromJson(json["merge_user"]),
        mergedAt: json["merged_at"] == null
            ? null
            : DateTime.parse(json["merged_at"]),
      );

  static Future<PaginatedAPIList<MergeRequest>> getMergeRequests({
    String state = 'opened',
    String scope = 'all',
    int? reviewerId,
    String? reviewerUsername,
    int? page,
    int? perPage,
  }) async {
    try {
      Map<String, String?> params = {
        'state': state,
        'scope': scope,
        'reviewer_id': reviewerId?.toString(),
        'reviewer_username': reviewerUsername,
        'order_by': 'updated_at',
        'sort': 'desc',
        'page': page?.toString(),
        'per_page': perPage?.toString(),
      }..removeWhere((key, value) => value == null);
      var url = Uri.https(ApiConstants.baseUrl,
          '${ApiConstants.versionPath}/merge_requests', params);
      var response = await http
          .get(url, headers: {'Authorization': await AuthService.authHeader()});
      if (response.statusCode == 200) {
        List<MergeRequest> data =
            mergeRequestsFromJson(utf8.decode(response.bodyBytes));
        return PaginatedAPIList<MergeRequest>(
          data,
          response.headers.containsKey('x-total')
              ? int.parse(response.headers['x-total']!)
              : data.length,
        );
      }
    } catch (e) {
      print(e.toString());
    }
    return PaginatedAPIList.empty();
  }

  static Future<MergeRequestApprovalStatus?> getApprovals(
    MergeRequest mergeRequest,
  ) async {
    try {
      var url = Uri.https(
        ApiConstants.baseUrl,
        urlTemplate(
          _Urls.approvals,
          {
            ':project': mergeRequest.projectId.toString(),
            ':mr': mergeRequest.iid.toString(),
          },
        ),
      );
      var response = await http.get(
        url,
        headers: {'Authorization': await AuthService.authHeader()},
      );
      if (response.statusCode == 200) {
        MergeRequestApprovalStatus data =
            mergeRequestApprovalsFromJson(response.body);
        return data;
      }
    } catch (e) {
      print(e.toString());
    }
    return null;
  }

  static Future<MergeRequestApprovalRuleStatus?> getApprovalRuleStatus(
      MergeRequest mergeRequest) async {
    try {
      var url = Uri.https(
        ApiConstants.baseUrl,
        urlTemplate(
          _Urls.approvalRuleStatus,
          {
            ':project': mergeRequest.projectId.toString(),
            ':mr': mergeRequest.iid.toString(),
          },
        ),
      );
      var response = await http.get(
        url,
        headers: {'Authorization': await AuthService.authHeader()},
      );
      if (response.statusCode == 200) {
        MergeRequestApprovalRuleStatus data =
            MergeRequestApprovalRuleStatus.fromJson(json.decode(response.body));
        return data;
      }
    } catch (e) {
      print(e.toString());
    }
    return null;
  }

  static Future<MergeRequest?> approve(MergeRequest mergeRequest) async {
    try {
      var url = Uri.https(
        ApiConstants.baseUrl,
        urlTemplate(
          _Urls.approve,
          {
            ':project': mergeRequest.projectId.toString(),
            ':mr': mergeRequest.iid.toString(),
          },
        ),
      );
      var response = await http.post(
        url,
        headers: {'Authorization': await AuthService.authHeader()},
      );
      if (response.statusCode == 200) {
        MergeRequest data = mergeRequestFromJson(response.body);
        return data;
      }
    } catch (e) {
      print(e.toString());
    }
    return null;
  }

  static Future<MergeRequest?> unapprove(MergeRequest mergeRequest) async {
    try {
      var url = Uri.https(
        ApiConstants.baseUrl,
        urlTemplate(
          _Urls.unapprove,
          {
            ':project': mergeRequest.projectId.toString(),
            ':mr': mergeRequest.iid.toString(),
          },
        ),
      );
      var response = await http.post(
        url,
        headers: {'Authorization': await AuthService.authHeader()},
      );
      if (response.statusCode == 200) {
        MergeRequest data = mergeRequestFromJson(response.body);
        return data;
      }
    } catch (e) {
      print(e.toString());
    }
    return null;
  }

  static Future<MergeRequest?> merge(
    MergeRequest mergeRequest, {
    bool? removeSourceBranch,
    bool? squash,
  }) async {
    try {
      var url = Uri.https(
        ApiConstants.baseUrl,
        urlTemplate(
          _Urls.merge,
          {
            ':project': mergeRequest.projectId.toString(),
            ':mr': mergeRequest.iid.toString(),
          },
        ),
      );
      var response = await http.put(
        url,
        headers: {'Authorization': await AuthService.authHeader()},
        body: {
          'should_remove_source_branch': removeSourceBranch,
          'squash': squash,
        }..removeWhere((key, value) => value == null),
      );
      if (response.statusCode == 200) {
        MergeRequest data = mergeRequestFromJson(response.body);
        return data;
      }
    } catch (e) {
      print(e.toString());
    }
    return null;
  }

  static Future<Pipeline?> getLatestPipeline(MergeRequest mr) async {
    return ApiService().getLatestPipeline(
      mr.projectId,
      ref: 'refs/merge-requests/${mr.iid}/head',
    );
  }

  static Future<List<Commit>> getCommits(MergeRequest mr) async {
    try {
      var url = Uri.https(
        ApiConstants.baseUrl,
        urlTemplate(
          _Urls.commits,
          {
            ':project': mr.projectId.toString(),
            ':mr': mr.iid.toString(),
          },
        ),
      );
      var response = await http.get(
        url,
        headers: {'Authorization': await AuthService.authHeader()},
      );
      if (response.statusCode == 200) {
        List<Commit> data = commitsFromJson(response.body);
        return data;
      }
    } catch (e) {
      print(e.toString());
    }
    return List.empty();
  }
}

class MergeRequestApprovalStatus extends _BaseMergeRequest {
  final int approvalsRequired;
  final int approvalsLeft;
  final List<User> approvedBy;

  MergeRequestApprovalStatus({
    id,
    iid,
    projectId,
    title,
    description,
    state,
    createdAt,
    updatedAt,
    detailedMergeStatus,
    required this.approvalsRequired,
    required this.approvalsLeft,
    required this.approvedBy,
  }) : super(
          id: id,
          iid: iid,
          projectId: projectId,
          title: title,
          description: description,
          state: state,
          createdAt: createdAt,
          updatedAt: updatedAt,
          detailedMergeStatus: detailedMergeStatus,
        );

  factory MergeRequestApprovalStatus.fromJson(Map<String, dynamic> data) =>
      MergeRequestApprovalStatus(
        id: data["id"],
        iid: data["iid"],
        projectId: data["project_id"],
        title: data["title"],
        description: data["description"],
        state: data['state'],
        createdAt: DateTime.parse(data["created_at"]),
        updatedAt: DateTime.parse(data["updated_at"]),
        detailedMergeStatus:
            MergeStatus.fromString(data["detailed_merge_status"]),
        approvalsRequired: data['approvals_required'],
        approvalsLeft: data['approvals_left'],
        approvedBy:
            MergeRequestApprovalStatus.approvaedByfromJson(data['approved_by']),
      );
  static List<User> approvaedByfromJson(List<dynamic> data) =>
      data.map((obj) => User.fromJson(obj['user'])).toList();
}

class MergeRequestApprovalRule {
  final int id;
  final String name;
  final String ruleType;
  final int approvalsRequired;
  final List<User> eligibleApprovers;
  final List<User> users;
  final List<User>? approvedBy;
  final bool? approved;

  MergeRequestApprovalRule({
    required this.id,
    required this.name,
    required this.ruleType,
    required this.approvalsRequired,
    required this.eligibleApprovers,
    required this.users,
    this.approvedBy,
    this.approved,
  });

  factory MergeRequestApprovalRule.fromJson(Map<String, dynamic> data) {
    inspect(data);
    return MergeRequestApprovalRule(
      id: data["id"],
      name: data["name"],
      ruleType: data["rule_type"],
      approvalsRequired: data["approvals_required"],
      eligibleApprovers: usersFromJson(json.encode(data['eligible_approvers'])),
      users: usersFromJson(json.encode(data['users'])),
      approvedBy: usersFromJson(json.encode(data['approved_by'])),
      approved: data['approved'],
    );
  }
}

class MergeRequestApprovalRuleStatus {
  final bool approvalRulesOverwritten;
  final List<MergeRequestApprovalRule> rules;

  MergeRequestApprovalRuleStatus({
    required this.approvalRulesOverwritten,
    required this.rules,
  });

  factory MergeRequestApprovalRuleStatus.fromJson(Map<String, dynamic> data) =>
      MergeRequestApprovalRuleStatus(
        approvalRulesOverwritten: data['approval_rules_overwritten'],
        rules: mergeRequestApprovalRulesFromJson(json.encode(data['rules'])),
      );
}

enum MergeStatus {
  blocked('blocked_status'),
  broken('broken_status'),
  checking('checking'),
  unchecked('unchecked'),
  ciMustPass('ci_must_pass'),
  ciStillRunning('ci_still_running'),
  discussionsNotResolved('discussions_not_resolved'),
  draft('draft_status'),
  externalStatusChecks('external_status_checks'),
  mergeable('mergeable'),
  notApproved('not_approved'),
  notOpen('not_open'),
  policiesDenied('policies_denied');

  final String value;
  const MergeStatus(this.value);

  factory MergeStatus.fromString(String val) =>
      MergeStatus.values.firstWhere((element) => element.value == val);
}
