import 'package:flutter/material.dart';
import 'package:gitlab/merge_requests/merge_request_model.dart';
import 'package:gitlab/users/widgets/avatar.dart';
import 'package:timeago/timeago.dart' as timeago;

import '../../commit/commit_model.dart';

class MergeRequestCommits extends StatefulWidget {
  final MergeRequest mergeRequest;
  const MergeRequestCommits(this.mergeRequest, {super.key});

  @override
  State<MergeRequestCommits> createState() => _MergeRequestCommitsState();
}

class _MergeRequestCommitsState extends State<MergeRequestCommits> {
  late Future<List<Commit>> _commits;

  @override
  void initState() {
    _commits = MergeRequest.getCommits(widget.mergeRequest);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _commits,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(child: CircularProgressIndicator());
        }
        return ListView.builder(
          shrinkWrap: true,
          itemCount: snapshot.data!.length,
          itemBuilder: (context, index) {
            var commit = snapshot.data![index];
            return ListTile(
              leading: UserAvatarByName(
                  commit.authorName, widget.mergeRequest.projectId),
              title: Text(commit.title),
              subtitle: Text(
                '${commit.authorName} authored ${timeago.format(commit.createdAt)}',
              ),
            );
          },
        );
      },
    );
  }
}
