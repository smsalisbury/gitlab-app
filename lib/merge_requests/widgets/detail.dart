import 'package:flutter/material.dart';
import 'package:gitlab/merge_requests/widgets/commits.dart';
import 'package:gitlab/merge_requests/widgets/detail_overview.dart';
import 'package:gitlab/merge_requests/widgets/state.dart';
import 'package:gitlab/common/text.dart';
import 'package:timeago/timeago.dart' as timeago;

import '../../users/widgets/avatar.dart';
import '../merge_request_model.dart';

class MergeRequestDetailWidget extends StatelessWidget {
  final MergeRequest mergeRequest;

  const MergeRequestDetailWidget(this.mergeRequest, {super.key});

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () => Future.sync(() => {}),
      child: DefaultTabController(
        length: 4,
        child: Scaffold(
          body: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  automaticallyImplyLeading: false,
                  title: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: TitleText(text: mergeRequest.title),
                      ),
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: [
                            MergeRequestStateWidget(mergeRequest, size: 12),
                            const SizedBox(width: 6),
                            SubtitleText(
                              text:
                                  'Created ${timeago.format(mergeRequest.createdAt)} ago by',
                            ),
                            const SizedBox(width: 4),
                            UserAvatar(user: mergeRequest.author, size: 20),
                            const SizedBox(width: 4),
                            SubtitleText(text: mergeRequest.author.name),
                          ],
                        ),
                      ),
                    ],
                  ),
                  floating: true,
                  pinned: true,
                  snap: true,
                  bottom: const TabBar(
                    tabs: [
                      Tab(text: 'Overview'),
                      Tab(text: 'Commits'),
                      Tab(text: 'Pipelines'),
                      Tab(text: 'Changes'),
                    ],
                  ),
                ),
              ];
            },
            body: TabBarView(
              children: <Widget>[
                MergeRequestDetailOverviewWidget(mergeRequest),
                MergeRequestCommits(mergeRequest),
                ListView(
                  children: const [Placeholder(), Placeholder(), Placeholder()],
                ),
                ListView(
                  children: const [Placeholder(), Placeholder(), Placeholder()],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
