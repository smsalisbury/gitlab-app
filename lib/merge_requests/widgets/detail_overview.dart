import 'package:flutter/material.dart';
import 'package:gitlab/merge_requests/widgets/overview/reports.dart';
import 'package:gitlab/widgets/markdown/gitlab_markdown.dart';

import '../../api_service.dart';
import '../../projects/project_model.dart';
import '../merge_request_model.dart';
import 'overview/approvals.dart';
import 'overview/merge.dart';
import 'overview/pipeline.dart';

const _sectionPadding = EdgeInsets.symmetric(vertical: 8, horizontal: 16);

class MergeRequestDetailOverviewWidget extends StatefulWidget {
  final MergeRequest mergeRequest;
  const MergeRequestDetailOverviewWidget(this.mergeRequest, {super.key});

  @override
  State<MergeRequestDetailOverviewWidget> createState() =>
      _MergeRequestDetailOverviewWidgetState();
}

class _MergeRequestDetailOverviewWidgetState
    extends State<MergeRequestDetailOverviewWidget> {
  late Project? project;

  @override
  void initState() {
    super.initState();
    project = null;
    getData();
  }

  void getData() async {
    project = (await ApiService().getProject(widget.mergeRequest.projectId));
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      children: _addDividers([
        Padding(
          padding: _sectionPadding.copyWith(top: 16),
          child: project == null
              ? const Placeholder()
              : GitLabMarkdown(
                  widget.mergeRequest.description,
                  projectId: project?.id,
                ),
        ),
        _MergeRequestBranches(widget.mergeRequest),
        MergeRequestLatestPipeline(widget.mergeRequest),
        MergeRequestApprovals(widget.mergeRequest),
        MergeRequestReports(widget.mergeRequest),
        MergeRequestMerge(widget.mergeRequest),
        const Placeholder(),
      ]),
    );
  }
}

class _MergeRequestBranches extends StatelessWidget {
  final MergeRequest mergeRequest;
  const _MergeRequestBranches(this.mergeRequest);

  @override
  Widget build(BuildContext context) {
    var branchStyle = TextStyle(
      fontFamily: 'RobotoMono',
      backgroundColor: Theme.of(context).highlightColor,
    );
    return Padding(
      padding: _sectionPadding,
      child: Row(
        children: [
          const Icon(Icons.merge),
          const SizedBox(width: 16),
          Expanded(
            child: RichText(
              text: TextSpan(
                text: 'Request to merge ',
                style: DefaultTextStyle.of(context).style,
                children: [
                  TextSpan(text: mergeRequest.sourceBranch, style: branchStyle),
                  const TextSpan(text: ' into '),
                  TextSpan(text: mergeRequest.targetBranch, style: branchStyle),
                  const TextSpan(text: '.'),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

List<Widget> _addDividers(List<Widget> widgets) {
  var list = widgets.expand((element) => [element, const Divider()]).toList();
  list.length = list.length - 1;
  return list;
}
