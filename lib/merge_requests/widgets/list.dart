import 'package:flutter/material.dart';
import 'package:gitlab/merge_requests/merge_request_model.dart';
import 'package:gitlab/merge_requests/widgets/list_tile.dart';
import 'package:gitlab/widgets/loader/list.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import '../../api_service.dart';

class MergeRequestListWidget extends StatefulWidget {
  const MergeRequestListWidget({super.key});

  @override
  State<MergeRequestListWidget> createState() => MergeRequestListWidgetState();
}

class MergeRequestListWidgetState<T extends MergeRequestListWidget>
    extends State<T> {
  final pageSize = 20;
  final PagingController<int, MergeRequest> pagingController =
      PagingController(firstPageKey: 0);

  @override
  void initState() {
    super.initState();
    pagingController.addPageRequestListener((pageKey) => fetchPage(pageKey));
  }

  Future<void> fetchPage(int pageKey) async {
    try {
      var page = (pageKey / pageSize).floor() + 1;
      var list = await getMergeRequests(page, pageSize);
      print(
          'fetch page | key: $pageKey, page: $page, length: ${list.data.length}, total: ${list.total}');
      final isLastPage = pageKey + list.data.length == list.total;
      if (isLastPage) {
        pagingController.appendLastPage(list.data);
      } else {
        final nextPageKey = pageKey + list.data.length;
        pagingController.appendPage(list.data, nextPageKey);
      }
    } catch (error) {
      pagingController.error = error;
    }
  }

  Future<PaginatedAPIList<MergeRequest>> getMergeRequests(
      int page, int size) async {
    return MergeRequest.getMergeRequests(
      page: page,
      perPage: pageSize,
    );
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () => Future.sync(() => pagingController.refresh()),
      child: PagedListView(
        pagingController: pagingController,
        builderDelegate: PagedChildBuilderDelegate<MergeRequest>(
          itemBuilder: (context, item, index) =>
              MergeRequestListTile(mergeRequest: item),
          firstPageProgressIndicatorBuilder: (context) => const SizedBox(
            width: double.infinity,
            height: 0,
            child: SkeletonListView(),
          ),
          newPageProgressIndicatorBuilder: (context) => const SizedBox(
            width: double.infinity,
            height: 72,
            child: SkeletonListView(itemCount: 1),
          ),
        ),
      ),
    );
  }
}
