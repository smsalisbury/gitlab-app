import 'package:flutter/material.dart';
import 'package:gitlab/merge_requests/merge_request_model.dart';
import 'package:gitlab/merge_requests/widgets/detail.dart';
import 'package:gitlab/users/widgets/avatar.dart';

import '../../api_service.dart';
import '../../projects/project_model.dart';

class MergeRequestListTile extends StatefulWidget {
  final MergeRequest mergeRequest;

  const MergeRequestListTile({super.key, required this.mergeRequest});

  @override
  State<MergeRequestListTile> createState() => _MergeRequestListTileState();
}

class _MergeRequestListTileState extends State<MergeRequestListTile> {
  late Project? project;

  @override
  void initState() {
    super.initState();
    project = null;
    getData();
  }

  void getData() async {
    project = (await ApiService().getProject(widget.mergeRequest.projectId));
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: UserAvatar(user: widget.mergeRequest.author),
      title: Text(
        widget.mergeRequest.title,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      ),
      subtitle: Row(
        children: [
          if (project != null) Text(project!.name),
          if (widget.mergeRequest.milestone != null) ...[
            const SizedBox(width: 8),
            Row(children: [
              const Icon(Icons.timer, size: 16),
              const SizedBox(width: 2),
              Text(widget.mergeRequest.milestone!.title)
            ])
          ],
        ],
      ),
      onTap: () {
        Navigator.push(context, MergeRequestDetailRoute(widget.mergeRequest));
      },
    );
  }
}

class MergeRequestDetailRoute extends MaterialPageRoute<void> {
  MergeRequestDetailRoute(MergeRequest mergeRequest)
      : super(builder: (BuildContext context) {
          return Scaffold(
            appBar: AppBar(title: Text(mergeRequest.references.short)),
            body: MergeRequestDetailWidget(mergeRequest),
          );
        });
}
