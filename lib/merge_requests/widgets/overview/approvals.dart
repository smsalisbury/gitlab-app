import 'package:flutter/material.dart';
import 'package:gitlab/merge_requests/merge_request_model.dart';

import '../../../api_service.dart';
import '../../../users/user_model.dart';
import '../../../users/widgets/avatar.dart';

class MergeRequestApprovals extends StatefulWidget {
  final MergeRequest mergeRequest;
  const MergeRequestApprovals(this.mergeRequest, {super.key});

  @override
  State<MergeRequestApprovals> createState() => _MergeRequestApprovalsState();
}

class _MergeRequestApprovalsState extends State<MergeRequestApprovals> {
  late Future<MergeRequestApprovalRuleStatus?> _rules;

  @override
  void initState() {
    refresh();
    super.initState();
  }

  void refresh() {
    setState(() {
      _rules = MergeRequest.getApprovalRuleStatus(widget.mergeRequest);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      child: Row(
        children: [
          const Icon(Icons.verified_user_outlined),
          const SizedBox(width: 16),
          Expanded(
            child: FutureBuilder(
              future: _rules,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const LinearProgressIndicator();
                } else if (!snapshot.hasData || snapshot.data!.rules.isEmpty) {
                  return _OptionalApproval(widget.mergeRequest);
                }
                return _ApprovalRules(
                  widget.mergeRequest,
                  snapshot.data!,
                  callback: () => refresh(),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}

class _OptionalApproval extends StatefulWidget {
  final MergeRequest mergeRequest;

  const _OptionalApproval(this.mergeRequest);

  @override
  State<_OptionalApproval> createState() => _OptionalApprovalState();
}

class _OptionalApprovalState extends State<_OptionalApproval> {
  late Future<MergeRequestApprovalStatus?> _approvals;

  @override
  void initState() {
    refresh();
    super.initState();
  }

  void refresh() {
    setState(() {
      _approvals = MergeRequest.getApprovals(widget.mergeRequest);
    });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _approvals,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const LinearProgressIndicator();
        }
        return Row(
          children: [
            Expanded(
              child: snapshot.hasData && snapshot.data!.approvedBy.isNotEmpty
                  ? Row(children: [
                      const Text('Approved by:'),
                      const SizedBox(width: 4),
                      ...snapshot.data!.approvedBy
                          .map((u) => UserAvatar(user: u, size: 24))
                          .toList(),
                    ])
                  : const Text('Approval is optional.'),
            ),
            _ApprovalButton(
              widget.mergeRequest,
              snapshot.data!.approvedBy,
              callback: () => refresh(),
            ),
          ],
        );
      },
    );
  }
}

class _ApprovalRules extends StatelessWidget {
  final MergeRequest mergeRequest;
  final MergeRequestApprovalRuleStatus status;
  final Function()? callback;
  const _ApprovalRules(
    this.mergeRequest,
    this.status, {
    this.callback,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        ListView.builder(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: status.rules.length,
          itemBuilder: (context, index) {
            const padding = EdgeInsets.all(12);
            var rule = status.rules[index];
            return Card(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: rule.eligibleApprovers.isEmpty
                        ? padding
                        : padding.copyWith(bottom: 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            if (rule.approved == true)
                              const Icon(Icons.check, color: Colors.green),
                            if (rule.approved == true) const SizedBox(width: 4),
                            Text(_approvalRuleName(rule)),
                          ],
                        ),
                        Text(
                          rule.approvalsRequired > 0
                              ? '${rule.approvedBy?.length ?? 0} of ${rule.approvalsRequired}'
                              : 'Optional',
                          style: const TextStyle(fontStyle: FontStyle.italic),
                        ),
                      ],
                    ),
                  ),
                  if (rule.eligibleApprovers.isNotEmpty) const Divider(),
                  if (rule.eligibleApprovers.isNotEmpty)
                    Padding(
                      padding: padding.copyWith(top: 0),
                      child: Row(
                        children: rule.eligibleApprovers
                            .map((user) => Padding(
                                  padding: const EdgeInsets.only(right: 2),
                                  child: UserAvatar(
                                    user: user,
                                    size: 24,
                                    opacity: rule.approvedBy?.any((approver) =>
                                                approver.id == user.id) ==
                                            true
                                        ? 1.0
                                        : 0.3,
                                  ),
                                ))
                            .toList(),
                      ),
                    )
                ],
              ),
            );
          },
        ),
        _ApprovalButton(
          mergeRequest,
          status.rules
              .expand((rule) => rule.approvedBy ?? [] as List<User>)
              .toList(),
          callback: callback,
        ),
      ],
    );
  }

  String _approvalRuleName(MergeRequestApprovalRule rule) {
    var plural = rule.eligibleApprovers.length != 1;
    if (rule.ruleType == 'code_owner') {
      return 'Code ${plural ? 'owners' : 'owner'} of ${rule.name}';
    }
    return rule.name;
  }
}

class _ApprovalButton extends StatefulWidget {
  final MergeRequest mergeRequest;
  final List<User> approvedBy;
  final Function()? callback;
  const _ApprovalButton(
    this.mergeRequest,
    this.approvedBy, {
    this.callback,
  });

  @override
  State<_ApprovalButton> createState() => _ApprovalButtonState();
}

class _ApprovalButtonState extends State<_ApprovalButton> {
  late Future<User?> _me;
  bool loader = false;

  @override
  void initState() {
    _me = ApiService().getUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _me,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting || loader) {
          return const _LoadingButton();
        } else if (snapshot.hasData) {
          var userApproved =
              widget.approvedBy.any((user) => user.id == snapshot.data!.id);
          return OutlinedButton(
            onPressed: () async => await handleTouch(userApproved),
            child: Text(userApproved ? 'Revoke approval' : 'Approve'),
          );
        }
        return const OutlinedButton(
          onPressed: null,
          child: Text('Approval not available'),
        );
      },
    );
  }

  Future<void> handleTouch(bool unapprove) async {
    setState(() => loader = true);
    SnackBar snackbar;
    if (unapprove) {
      snackbar = const SnackBar(content: Text('Approval revoked'));
      await MergeRequest.unapprove(widget.mergeRequest);
    } else {
      snackbar = const SnackBar(content: Text('Merge request approved'));
      await MergeRequest.approve(widget.mergeRequest);
    }
    if (context.mounted) ScaffoldMessenger.of(context).showSnackBar(snackbar);
    if (widget.callback != null) widget.callback!();
    setState(() => loader = false);
  }
}

class _LoadingButton extends StatelessWidget {
  const _LoadingButton();

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      onPressed: null,
      child: Container(
        width: 24,
        height: 24,
        padding: const EdgeInsets.all(2),
        child: const CircularProgressIndicator(
          strokeWidth: 2,
        ),
      ),
    );
  }
}
