import 'package:flutter/material.dart';
import 'package:gitlab/merge_requests/merge_request_model.dart';
import 'package:gitlab/users/widgets/avatar.dart';
import 'package:timeago/timeago.dart' as timeago;

enum Options { deleteSource, squashCommits }

class MergeRequestMerge extends StatelessWidget {
  final MergeRequest mergeRequest;

  const MergeRequestMerge(this.mergeRequest, {super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      child: Row(
        children: [
          _getIcon(),
          const SizedBox(width: 16),
          Expanded(child: _getMergeWidget())
        ],
      ),
    );
  }

  Icon _getIcon() {
    switch (mergeRequest.detailedMergeStatus) {
      case MergeStatus.mergeable:
        return const Icon(Icons.merge, color: Colors.green);
      case MergeStatus.notOpen:
        return mergeRequest.state == 'merged'
            ? const Icon(Icons.merge, color: Colors.green)
            : const Icon(Icons.report, color: Colors.red);
      default:
        return const Icon(Icons.warning, color: Colors.orange);
    }
  }

  Widget _getMergeWidget() {
    switch (mergeRequest.detailedMergeStatus) {
      case MergeStatus.mergeable:
        return _ReadyToMerge(mergeRequest);
      case MergeStatus.notOpen:
        return _NotOpen(mergeRequest);
      case MergeStatus.notApproved:
        return const _BlockedWithReason(
            'all required approvals must be given.');
      default:
        return Text(
            'Unhandled merge status: ${mergeRequest.detailedMergeStatus}');
    }
  }
}

class _ReadyToMerge extends StatefulWidget {
  final MergeRequest mergeRequest;

  const _ReadyToMerge(this.mergeRequest);

  @override
  State<_ReadyToMerge> createState() => _ReadyToMergeState();
}

class _ReadyToMergeState extends State<_ReadyToMerge> {
  Set<Options> _selectedOptions = {};
  bool loader = false;
  MergeRequest? savedMr;

  @override
  void initState() {
    setState(() {
      if (widget.mergeRequest.shouldRemoveSourceBranch == true ||
          widget.mergeRequest.forceRemoveSourceBranch == true) {
        _selectedOptions.add(Options.deleteSource);
      }
      if (widget.mergeRequest.squashOnMerge == true) {
        _selectedOptions.add(Options.squashCommits);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Text('Ready to merge!',
            style: TextStyle(fontWeight: FontWeight.bold)),
        const SizedBox(height: 8),
        SegmentedButton(
          multiSelectionEnabled: true,
          emptySelectionAllowed: true,
          style: const ButtonStyle(visualDensity: VisualDensity.compact),
          segments: const [
            ButtonSegment(
              value: Options.deleteSource,
              label: Text('Delete branch'),
              icon: Icon(Icons.delete),
            ),
            ButtonSegment(
              value: Options.squashCommits,
              label: Text('Squash commits'),
              icon: Icon(Icons.unfold_less_double),
            ),
          ],
          selected: _selectedOptions,
          onSelectionChanged: (newSelection) {
            setState(() {
              _selectedOptions = newSelection;
            });
          },
        ),
        const SizedBox(height: 8),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            loader
                ? const _LoadingButton()
                : FilledButton(
                    onPressed: () => _merge(),
                    child: const Text('Merge'),
                  ),
          ],
        )
      ],
    );
  }

  void _merge() async {
    setState(() => loader = true);
    SnackBar snackbar = const SnackBar(content: Text('Merge request merged'));
    savedMr = await MergeRequest.merge(
      widget.mergeRequest,
      removeSourceBranch: _selectedOptions.contains(Options.deleteSource),
      squash: _selectedOptions.contains(Options.squashCommits),
    );
    if (context.mounted) ScaffoldMessenger.of(context).showSnackBar(snackbar);
    setState(() => loader = false);
  }
}

class _NotOpen extends StatelessWidget {
  final MergeRequest mergeRequest;
  const _NotOpen(this.mergeRequest);

  @override
  Widget build(BuildContext context) {
    if (mergeRequest.state == 'merged') {
      return RichText(
        text: TextSpan(
          style: DefaultTextStyle.of(context).style,
          children: [
            const TextSpan(
              text: 'Merged by ',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            WidgetSpan(
              child: UserAvatar(user: mergeRequest.mergeUser!, size: 24),
              alignment: PlaceholderAlignment.middle,
            ),
            TextSpan(
              text: ' ${mergeRequest.mergeUser!.name}',
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text: ' ${timeago.format(mergeRequest.mergedAt!)}',
            ),
          ],
        ),
      );
    }
    return const Text('Merge request is not open',
        style: TextStyle(fontStyle: FontStyle.italic));
  }
}

class _BlockedWithReason extends StatelessWidget {
  final String reason;
  const _BlockedWithReason(this.reason);

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        style: DefaultTextStyle.of(context).style,
        children: [
          const TextSpan(
            text: 'Merge blocked: ',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          TextSpan(
            text: reason,
          ),
        ],
      ),
    );
  }
}

class _LoadingButton extends StatelessWidget {
  const _LoadingButton();

  @override
  Widget build(BuildContext context) {
    return FilledButton(
      onPressed: null,
      child: Container(
        width: 24,
        height: 24,
        padding: const EdgeInsets.all(2),
        child: const CircularProgressIndicator(
          strokeWidth: 2,
        ),
      ),
    );
  }
}
