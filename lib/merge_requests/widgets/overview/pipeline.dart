import 'package:flutter/material.dart';
import 'package:gitlab/widgets/loader/shapes.dart';
import 'package:timeago/timeago.dart' as timeago;

import '../../../pipelines/pipeline_model.dart';
import '../../merge_request_model.dart';

class MergeRequestLatestPipeline extends StatefulWidget {
  final MergeRequest mergeRequest;
  const MergeRequestLatestPipeline(this.mergeRequest, {super.key});

  @override
  State<MergeRequestLatestPipeline> createState() =>
      _MergeRequestLatestPipelineState();
}

class _MergeRequestLatestPipelineState
    extends State<MergeRequestLatestPipeline> {
  late Future<Pipeline?> _pipeline;

  @override
  void initState() {
    _pipeline = MergeRequest.getLatestPipeline(widget.mergeRequest);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
        child: FutureBuilder(
          future: _pipeline,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const _Loader();
            }

            if (snapshot.data == null) {
              return Row(
                children: [
                  icon(),
                  const SizedBox(width: 16),
                  const Expanded(
                    child: Text(
                      'No pipelines in this MR',
                      style: TextStyle(fontStyle: FontStyle.italic),
                    ),
                  ),
                ],
              );
            }

            var pipeline = snapshot.data!;
            return Row(
              children: [
                icon(status: pipeline.status),
                const SizedBox(width: 16),
                Expanded(child: content(pipeline)),
                const SizedBox(width: 16),
                if (pipeline.finishedAt != null)
                  Text(timeago.format(pipeline.finishedAt!)),
              ],
            );
          },
        ));
  }

  Widget content(Pipeline pipeline) {
    switch (pipeline.status) {
      case 'success':
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Pipeline passed for ${pipeline.sha.substring(0, 8)}', // TODO: link commit and pipeline
            ),
            Text('Test coverage: ${pipeline.coverage}%'),
          ],
        );
      case 'failed':
        return Text(
          'Pipeline failed for ${pipeline.sha.substring(0, 8)}', // TODO: link commit and pipeline
        );
      default:
        return Text('Unhandled pipeline status: ${pipeline.status}');
    }
  }

  Widget icon({String? status}) {
    switch (status) {
      case 'success':
        return const Icon(Icons.check_circle_outline, color: Colors.green);
      case 'failed':
        return const Icon(Icons.cancel_outlined, color: Colors.red);
      default:
        return const Icon(Icons.remove_circle_outline, color: Colors.grey);
    }
  }
}

class _Loader extends StatelessWidget {
  const _Loader();

  @override
  Widget build(BuildContext context) {
    return const Row(
      children: [
        SkeletonIcon(),
        SizedBox(width: 16),
        Expanded(
          child: SkeletonParagraph(lines: 3),
        ),
      ],
    );
  }
}
