import 'package:flutter/material.dart';
import 'package:gitlab/merge_requests/merge_request_model.dart';

class MergeRequestReports extends StatelessWidget {
  final MergeRequest mergeRequest;
  const MergeRequestReports(this.mergeRequest, {super.key});

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      child: Row(
        children: [
          Icon(Icons.verified_user_outlined),
          SizedBox(width: 16),
          Expanded(child: Text('test'))
        ],
      ),
    );
  }
}
