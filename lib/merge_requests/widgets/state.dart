import 'package:flutter/material.dart';
import 'package:gitlab/merge_requests/merge_request_model.dart';

class MergeRequestStateWidget extends StatelessWidget {
  final MergeRequest mergeRequest;
  final double? size;

  const MergeRequestStateWidget(this.mergeRequest, {super.key, this.size});

  @override
  Widget build(BuildContext context) {
    return Chip(
      avatar: Icon(icon, color: fgColor, size: size),
      backgroundColor: bgColor,
      label: Text(text, style: TextStyle(color: fgColor, fontSize: size)),
      side: BorderSide.none,
      visualDensity: VisualDensity.compact,
    );
  }

  String get text {
    Map<String, String> stateMap = Map.from({
      'opened': 'Open',
      'locked': 'Locked',
      'closed': 'Closed',
      'merged': 'Merged',
    });
    return stateMap[mergeRequest.state] ?? mergeRequest.state;
  }

  Color get fgColor {
    Map<String, Color> stateMap = Map.from({
      'opened': Colors.green.shade600,
      'merged': Colors.blue.shade900,
    });
    return stateMap[mergeRequest.state] ?? Colors.black;
  }

  Color get bgColor {
    Map<String, Color> stateMap = Map.from({
      'opened': Colors.green.shade100,
      'merged': Colors.blue.shade100,
    });
    return stateMap[mergeRequest.state] ?? Colors.grey;
  }

  IconData get icon {
    Map<String, IconData> stateMap = Map.from({
      'opened': Icons.web_stories,
      'merged': Icons.merge,
    });
    return stateMap[mergeRequest.state] ?? Icons.abc;
  }
}
