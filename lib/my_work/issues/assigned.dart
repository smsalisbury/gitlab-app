import 'package:flutter/material.dart';
import 'package:gitlab/issues/widgets/list.dart';

import '../../api_service.dart';
import '../../issues/issue_model.dart';

class MyAssignedIssues extends IssueListWidget {
  const MyAssignedIssues({super.key});

  @override
  State<MyAssignedIssues> createState() => _MyAssignedIssuesState();
}

class _MyAssignedIssuesState extends IssueListWidgetState<MyAssignedIssues> {
  @override
  Future<PaginatedAPIList<Issue>> getIssues(int page, int size) async {
    return Issue.list(
      scope: 'assigned_to_me',
      state: 'opened',
      page: page,
      perPage: super.pageSize,
    );
  }
}
