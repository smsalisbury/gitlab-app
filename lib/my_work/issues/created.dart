import 'package:flutter/material.dart';
import 'package:gitlab/issues/widgets/list.dart';

import '../../api_service.dart';
import '../../issues/issue_model.dart';

class MyCreatedIssues extends IssueListWidget {
  const MyCreatedIssues({super.key});

  @override
  State<MyCreatedIssues> createState() => _MyCreatedIssuesState();
}

class _MyCreatedIssuesState extends IssueListWidgetState<MyCreatedIssues> {
  @override
  Future<PaginatedAPIList<Issue>> getIssues(int page, int size) async {
    return Issue.list(
      state: 'opened',
      page: page,
      perPage: super.pageSize,
    );
  }
}
