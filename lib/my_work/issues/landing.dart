import 'package:flutter/material.dart';
import 'package:gitlab/my_work/issues/assigned.dart';
import 'package:gitlab/my_work/issues/created.dart';
import 'package:gitlab/my_work/issues/reacted.dart';

class MyIssues extends StatefulWidget {
  const MyIssues({super.key});

  @override
  State<MyIssues> createState() => _MyIssuesState();
}

class _MyIssuesState extends State<MyIssues> {
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        body: [
          const MyAssignedIssues(),
          const MyCreatedIssues(),
          const MyReactedIssues(),
        ][_currentIndex],
        bottomNavigationBar: NavigationBar(
          onDestinationSelected: (int index) =>
              setState(() => _currentIndex = index),
          selectedIndex: _currentIndex,
          destinations: const [
            NavigationDestination(
                label: 'Assigned to me', icon: Icon(Icons.assignment_ind)),
            NavigationDestination(
                label: 'Created by me', icon: Icon(Icons.create)),
            NavigationDestination(label: 'Reacted', icon: Icon(Icons.thumb_up)),
          ],
        ),
      ),
    );
  }
}
