import 'package:flutter/material.dart';
import 'package:gitlab/issues/widgets/list.dart';

import '../../api_service.dart';
import '../../issues/issue_model.dart';

class MyReactedIssues extends IssueListWidget {
  const MyReactedIssues({super.key});

  @override
  State<MyReactedIssues> createState() => _MyReactedIssuesState();
}

class _MyReactedIssuesState extends IssueListWidgetState<MyReactedIssues> {
  @override
  Future<PaginatedAPIList<Issue>> getIssues(int page, int size) async {
    return Issue.list(
      scope: 'all',
      myReactionEmoji: 'Any',
      state: 'opened',
      page: page,
      perPage: super.pageSize,
    );
  }
}
