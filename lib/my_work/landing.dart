import 'package:flutter/material.dart';
import 'package:gitlab/api_service.dart';
import 'package:gitlab/my_work/issues/landing.dart';
import 'package:gitlab/my_work/merge_requests/landing.dart';
import 'package:gitlab/my_work/projects/landing.dart';
import 'package:gitlab/users/user_model.dart';

import '../auth_service.dart';
import '../users/widgets/avatar.dart';
import '../widgets/coming_soon.dart';

const List<Widget> widgets = [
  MyProjects(),
  Placeholder(),
  MyIssues(),
  MyMergeRequests(),
  Placeholder(),
  Placeholder(),
  Placeholder(),
];

class MyWorkLanding extends StatefulWidget {
  const MyWorkLanding({super.key});

  @override
  State<MyWorkLanding> createState() => _MyWorkLandingState();
}

class _MyWorkLandingState extends State<MyWorkLanding> {
  int _currentPage = 0;

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return Scaffold(
      appBar: const SearchAppBar(),
      drawer: NavigationDrawer(
        selectedIndex: _currentPage,
        onDestinationSelected: (value) {
          setState(() => _currentPage = value);
          Navigator.pop(context);
        },
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: SizedBox(
              height: 56.0,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    'MinGit',
                    style: theme.textTheme.displaySmall?.copyWith(
                      fontFamily: 'RobotoMono',
                    ),
                  ),
                ],
              ),
            ),
          ),
          const Divider(),
          const ListTile(
            leading: Icon(Icons.work_outline),
            title: Text('My Work'),
          ),
          const Divider(),
          const NavigationDrawerDestination(
            icon: Icon(Icons.group),
            label: Text('Projects'),
          ),
          const NavigationDrawerDestination(
            icon: Icon(Icons.calendar_month),
            label: ComingSoonLabel(child: Text('Groups')),
          ),
          const Divider(),
          const NavigationDrawerDestination(
            icon: Icon(Icons.group),
            label: Text('Issues'),
          ),
          const NavigationDrawerDestination(
            icon: Icon(Icons.calendar_month),
            label: Text('Merge Requests'),
          ),
          const NavigationDrawerDestination(
            icon: Icon(Icons.calendar_month),
            label: ComingSoonLabel(child: Text('To-Do List')),
          ),
          const Divider(),
          const NavigationDrawerDestination(
            icon: Icon(Icons.settings),
            label: ComingSoonLabel(child: Text('Settings')),
          ),
          const NavigationDrawerDestination(
            icon: Icon(Icons.help),
            label: ComingSoonLabel(child: Text('Help & Feedback')),
          ),
        ],
      ),
      body: widgets[_currentPage],
    );
  }
}

class SearchAppBar extends StatefulWidget implements PreferredSizeWidget {
  final double padding = 16.0;
  final double preferredHeight = kToolbarHeight + 32.0;

  const SearchAppBar({super.key});

  @override
  State<SearchAppBar> createState() => _SearchAppBarState();

  @override
  Size get preferredSize => Size.fromHeight(preferredHeight);
}

class _SearchAppBarState extends State<SearchAppBar> {
  late User? user;

  @override
  void initState() {
    user = null;
    super.initState();
    getData();
  }

  void getData() async {
    user = await ApiService().getUser();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    assert(debugCheckHasMediaQuery(context));

    var osToolbarHeight = MediaQuery.paddingOf(context).top;
    final ScaffoldState? scaffold = Scaffold.maybeOf(context);
    final bool hasDrawer = scaffold?.hasDrawer ?? false;

    Widget leading = const Icon(Icons.search);
    if (hasDrawer) {
      leading = DrawerButton(
        style: IconButton.styleFrom(iconSize: 24),
      );
    }

    return Padding(
      padding: EdgeInsets.only(
        top: osToolbarHeight + widget.padding,
        bottom: widget.padding,
        left: widget.padding,
        right: widget.padding,
      ),
      child: SearchBar(
        leading: leading,
        hintText: 'Search GitLab',
        trailing: [
          IconButton(
            onPressed: () async {
              await AuthService.login(context);
              // getData();
            },
            icon: user == null
                ? const Icon(Icons.person)
                : UserAvatar(
                    user: user!,
                    size: 30,
                  ),
          ),
        ],
      ),
    );
  }
}
