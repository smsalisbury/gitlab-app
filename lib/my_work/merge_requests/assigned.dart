import 'package:flutter/material.dart';
import 'package:gitlab/merge_requests/merge_request_model.dart';
import 'package:gitlab/merge_requests/widgets/list.dart';

import '../../api_service.dart';

class AssignedMergeRequestsWidget extends MergeRequestListWidget {
  const AssignedMergeRequestsWidget({super.key});

  @override
  State<AssignedMergeRequestsWidget> createState() =>
      _AssignedMergeRequestsWidgetState();
}

class _AssignedMergeRequestsWidgetState
    extends MergeRequestListWidgetState<AssignedMergeRequestsWidget> {
  @override
  Future<PaginatedAPIList<MergeRequest>> getMergeRequests(
    int page,
    int size,
  ) async {
    return MergeRequest.getMergeRequests(
      scope: 'assigned_to_me',
      state: 'opened',
      page: page,
      perPage: super.pageSize,
    );
  }
}
