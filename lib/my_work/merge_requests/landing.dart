import 'package:flutter/material.dart';
import 'package:gitlab/my_work/merge_requests/assigned.dart';
import 'package:gitlab/my_work/merge_requests/review.dart';

class MyMergeRequests extends StatefulWidget {
  const MyMergeRequests({super.key});

  @override
  State<MyMergeRequests> createState() => _MyMergeRequestsState();
}

class _MyMergeRequestsState extends State<MyMergeRequests> {
  int _currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        body: [
          const AssignedMergeRequestsWidget(),
          const ReviewMergeRequestsWidget(),
        ][_currentIndex],
        bottomNavigationBar: NavigationBar(
          onDestinationSelected: (int index) =>
              setState(() => _currentIndex = index),
          selectedIndex: _currentIndex,
          destinations: const [
            NavigationDestination(
              label: 'Assigned to me',
              icon: Icon(Icons.assignment_ind),
            ),
            NavigationDestination(
              label: 'Review reqeusts',
              icon: Icon(Icons.content_paste_search),
            )
          ],
        ),
      ),
    );
  }
}
