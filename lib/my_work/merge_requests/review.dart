import 'package:flutter/material.dart';
import 'package:gitlab/merge_requests/widgets/list.dart';

import '../../api_service.dart';
import '../../merge_requests/merge_request_model.dart';
import '../../users/user_model.dart';

class ReviewMergeRequestsWidget extends MergeRequestListWidget {
  const ReviewMergeRequestsWidget({super.key});

  @override
  State<ReviewMergeRequestsWidget> createState() =>
      _ReviewMergeRequestsWidgetState();
}

class _ReviewMergeRequestsWidgetState
    extends MergeRequestListWidgetState<ReviewMergeRequestsWidget> {
  @override
  Future<PaginatedAPIList<MergeRequest>> getMergeRequests(
    int page,
    int size,
  ) async {
    var user = await User.getMe();
    if (user == null) return PaginatedAPIList.empty();
    return MergeRequest.getMergeRequests(
      reviewerId: user.id,
      state: 'opened',
      page: page,
      perPage: super.pageSize,
    );
  }
}
