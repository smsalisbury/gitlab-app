import 'package:flutter/material.dart';
import 'package:gitlab/my_work/projects/membership.dart';
import 'package:gitlab/my_work/projects/personal.dart';
import 'package:gitlab/my_work/projects/starred.dart';

class MyProjects extends StatefulWidget {
  const MyProjects({super.key});

  @override
  State<MyProjects> createState() => _MyProjectsState();
}

class _MyProjectsState extends State<MyProjects> {
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: [
        const MembershipProjects(),
        const StarredProjects(),
        const PersonalProjects(),
      ][_currentIndex],
      bottomNavigationBar: NavigationBar(
        onDestinationSelected: (int index) =>
            setState(() => _currentIndex = index),
        selectedIndex: _currentIndex,
        destinations: const [
          NavigationDestination(
            icon: Icon(Icons.folder),
            label: 'My Projects',
          ),
          NavigationDestination(
            icon: Icon(Icons.star),
            label: 'Starred',
          ),
          NavigationDestination(
            icon: Icon(Icons.person),
            label: 'Personal Projects',
          ),
        ],
      ),
    );
  }
}
