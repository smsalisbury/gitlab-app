import 'package:flutter/material.dart';
import 'package:gitlab/projects/project_model.dart';

import '../../api_service.dart';
import '../../projects/widgets/list.dart';

class MembershipProjects extends ProjectListWidget {
  const MembershipProjects({super.key});

  @override
  State<MembershipProjects> createState() => _MembershipProjectsState();
}

class _MembershipProjectsState
    extends ProjectListWidgetState<MembershipProjects> {
  @override
  Future<PaginatedAPIList<Project>> getProjects(int page, int size) async {
    return Project.list(
      membership: true,
      simple: true,
      page: page,
      perPage: super.pageSize,
    );
  }
}
