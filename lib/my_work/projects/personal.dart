import 'package:flutter/material.dart';
import 'package:gitlab/projects/project_model.dart';
import 'package:gitlab/users/user_model.dart';

import '../../api_service.dart';
import '../../projects/widgets/list.dart';

class PersonalProjects extends ProjectListWidget {
  const PersonalProjects({super.key});

  @override
  State<PersonalProjects> createState() => _PersonalProjectsState();
}

class _PersonalProjectsState extends ProjectListWidgetState<PersonalProjects> {
  @override
  Future<PaginatedAPIList<Project>> getProjects(int page, int size) async {
    var user = await User.getMe();
    if (user == null) return PaginatedAPIList.empty();
    return Project.listUserProjects(
      user,
      simple: true,
      page: page,
      perPage: super.pageSize,
    );
  }
}
