import 'package:flutter/material.dart';
import 'package:gitlab/projects/project_model.dart';

import '../../api_service.dart';
import '../../projects/widgets/list.dart';

class StarredProjects extends ProjectListWidget {
  const StarredProjects({super.key});

  @override
  State<StarredProjects> createState() => _StarredProjectsState();
}

class _StarredProjectsState extends ProjectListWidgetState<StarredProjects> {
  @override
  Future<PaginatedAPIList<Project>> getProjects(int page, int size) async {
    return Project.list(
      starred: true,
      simple: true,
      page: page,
      perPage: super.pageSize,
    );
  }
}
