import 'dart:convert';

Pipeline pipelineFromJson(String str) => Pipeline.fromJson(json.decode(str));
List<Pipeline> pipelinesFromJson(String str) =>
    List<Pipeline>.from(json.decode(str).map((x) => Pipeline.fromJson(x)));

class Pipeline {
  final int id;
  final int iid;
  final int projectId;
  final String sha;
  final String ref;
  final String status;
  final String source;
  final String webUrl;
  final String? coverage;
  final DateTime createdAt;
  final DateTime updatedAt;
  final DateTime startedAt;
  final DateTime? finishedAt;

  Pipeline({
    required this.id,
    required this.iid,
    required this.projectId,
    required this.sha,
    required this.ref,
    required this.status,
    required this.source,
    required this.webUrl,
    required this.createdAt,
    required this.updatedAt,
    required this.startedAt,
    this.coverage,
    this.finishedAt,
  });

  factory Pipeline.fromJson(Map<String, dynamic> json) => Pipeline(
        id: json["id"],
        iid: json["iid"],
        projectId: json["project_id"],
        sha: json["sha"],
        ref: json["ref"],
        status: json["status"],
        source: json["source"],
        webUrl: json["web_url"],
        coverage: json["coverage"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        startedAt: DateTime.parse(json["updated_at"]),
        finishedAt: DateTime.parse(json["finished_at"]),
      );
}

class CodeQualityReportSummary {}
