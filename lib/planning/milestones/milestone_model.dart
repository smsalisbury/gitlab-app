import 'dart:convert';
import 'package:gitlab/api_service.dart';
import 'package:http/http.dart' as http;

import '../../auth_service.dart';
import '../../constants.dart';

Milestone milestoneFromJson(String str) => Milestone.fromJson(json.decode(str));
List<Milestone> milestonesFromJson(String str) =>
    List<Milestone>.from(json.decode(str).map((x) => Milestone.fromJson(x)));

class _Urls {
  static String listForProject =
      '${ApiConstants.versionPath}/projects/:id/milestones';
}

class Milestone {
  final int id;
  final String title;

  Milestone({
    required this.id,
    required this.title,
  });

  factory Milestone.fromJson(Map<String, dynamic> json) => Milestone(
        id: json["id"],
        title: json["title"],
      );

  static Future<List<Milestone>> listForProject(
    int projectId, {
    String? state,
    String? search,
    bool? includeParentMilestones,
  }) async {
    try {
      var params = {
        'state': state?.toString(),
        'search': search?.toString(),
        'include_parent_milestones': includeParentMilestones?.toString(),
      }..removeWhere((key, value) => value == null);
      var url = Uri.https(
        ApiConstants.baseUrl,
        urlTemplate(_Urls.listForProject, {':id': projectId.toString()}),
        params,
      );
      var response = await http
          .get(url, headers: {'Authorization': await AuthService.authHeader()});
      if (response.statusCode == 200) {
        List<Milestone> data = milestonesFromJson(response.body);
        return data;
      }
    } catch (e) {
      print(e.toString());
    }
    return [];
  }
}
