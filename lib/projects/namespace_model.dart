class Namespace {
  final int id;
  final String name;
  final String path;
  final String kind;
  final String fullPath;
  final String webUrl;
  final String? avatarUrl;
  final int? parentId;

  const Namespace(
      {required this.id,
      required this.name,
      required this.path,
      required this.kind,
      required this.fullPath,
      required this.webUrl,
      this.avatarUrl,
      this.parentId});

  factory Namespace.fromJson(Map<String, dynamic> json) => Namespace(
        id: json["id"],
        name: json["name"],
        path: json["path"],
        kind: json["kind"],
        fullPath: json["full_path"],
        webUrl: json["web_url"],
        avatarUrl: json["avatar_url"],
        parentId: json["parent_url"],
      );
}
