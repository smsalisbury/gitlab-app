import 'dart:convert';
import 'dart:developer';
import 'package:gitlab/api_service.dart';
import 'package:gitlab/users/user_model.dart';
import 'package:http/http.dart' as http;

import '../auth_service.dart';
import '../constants.dart';
import 'namespace_model.dart';

Project projectFromJson(String str) => Project.fromJson(json.decode(str));
List<Project> projectsFromJson(String str) =>
    List<Project>.from(json.decode(str).map((x) => Project.fromJson(x)));

final Map<String, Project?> cachedProjects = {};
final Map<String, User?> cachedMembers = {};

class _Urls {
  static String allMembers =
      '${ApiConstants.versionPath}/projects/:id/members/all';
  static String list = '${ApiConstants.versionPath}/projects';
  static String listForUser = '${ApiConstants.versionPath}/users/:id/projects';
  static String projectUsers = '${ApiConstants.versionPath}/projects/:id/users';
}

class Project {
  final int id;
  final String name;
  final String? nameWithNamespace;
  final String? pathWithNamespace;
  final String? avatarUrl;
  final String? visibility;
  final double? starCount;
  final double? openIssuesCount;
  final Namespace? namespace;

  Project({
    required this.id,
    required this.name,
    this.nameWithNamespace,
    this.pathWithNamespace,
    this.avatarUrl,
    this.visibility,
    this.starCount,
    this.openIssuesCount,
    this.namespace,
  });

  factory Project.fromJson(Map<String, dynamic> json) => Project(
        id: json["id"],
        name: json["name"],
        nameWithNamespace: json["name_with_namespace"],
        pathWithNamespace: json["path_with_namespace"],
        avatarUrl: json["avatar_url"],
        namespace: json["namespace"] == null
            ? null
            : Namespace.fromJson(json["namespace"]),
      );

  static Future<PaginatedAPIList<Project>> list({
    bool? membership,
    bool? starred,
    bool? simple,
    int? page,
    int? perPage,
  }) async {
    try {
      var params = {
        'membership': membership?.toString(),
        'starred': starred?.toString(),
        'simple': simple?.toString(),
        'order_by': 'updated_at',
        'sort': 'desc',
        'page': page?.toString(),
        'per_page': perPage?.toString(),
      }..removeWhere((key, value) => value == null);
      var url = Uri.https(ApiConstants.baseUrl, _Urls.list, params);
      var response = await http
          .get(url, headers: {'Authorization': await AuthService.authHeader()});
      if (response.statusCode == 200) {
        List<Project> data = projectsFromJson(response.body);
        return PaginatedAPIList<Project>(
          data,
          response.headers.containsKey('x-total')
              ? int.parse(response.headers['x-total']!)
              : data.length,
        );
      }
    } catch (e) {
      print(e.toString());
    }
    return PaginatedAPIList.empty();
  }

  static Future<PaginatedAPIList<Project>> listUserProjects(
    User user, {
    bool? simple,
    int? page,
    int? perPage,
  }) async {
    try {
      var params = {
        'order_by': 'updated_at',
        'sort': 'desc',
        'page': page?.toString(),
        'per_page': perPage?.toString(),
      }..removeWhere((key, value) => value == null);
      var url = Uri.https(
        ApiConstants.baseUrl,
        urlTemplate(_Urls.listForUser, {':id': user.id.toString()}),
        params,
      );
      var response = await http
          .get(url, headers: {'Authorization': await AuthService.authHeader()});
      if (response.statusCode == 200) {
        List<Project> data = projectsFromJson(response.body);
        return PaginatedAPIList<Project>(
          data,
          response.headers.containsKey('x-total')
              ? int.parse(response.headers['x-total']!)
              : data.length,
        );
      }
    } catch (e) {
      print(e.toString());
    }
    return PaginatedAPIList.empty();
  }

  static Future<Project?> get({
    int? id,
    String? path,
    bool cached = true,
  }) async {
    assert(id != null || path != null, 'One of id or path must be specified');

    var identifier = id?.toString() ?? Uri.encodeComponent(path!);
    if (cached && cachedProjects.containsKey(identifier)) {
      print('Using cache for $identifier');
      return cachedProjects[identifier];
    }

    try {
      var url = Uri.https(
          ApiConstants.baseUrl, '${ApiConstants.projects}/$identifier');
      var response = await http
          .get(url, headers: {'Authorization': await AuthService.authHeader()});
      if (response.statusCode == 200) {
        Project model = projectFromJson(response.body);
        cachedProjects[identifier] = model;
        return model;
      }
    } catch (e) {
      print(e.toString());
    }

    return null;
  }

  static Future<User?> getUserByName(String name, int project,
      {bool cached = true}) async {
    var cacheKey = '$project-$name';
    if (cached && cachedMembers.containsKey(cacheKey)) {
      return cachedMembers[cacheKey];
    }

    try {
      var url = Uri.https(
        ApiConstants.baseUrl,
        urlTemplate(_Urls.allMembers, {':id': project.toString()}),
        {'query': name},
      );
      var response = await http.get(
        url,
        headers: {'Authorization': await AuthService.authHeader()},
      );
      if (response.statusCode == 200) {
        List<User> data = usersFromJson(response.body);
        cachedMembers[cacheKey] = data.isEmpty ? null : data[0];
        return data.isEmpty ? null : data[0];
      }
    } catch (e) {
      log(e.toString());
    }

    return null;
  }

  static Future<List<User>> getMembers(
    int project, {
    String? query,
    List<int>? userIds,
    bool? showSeatInfo,
    String? state,
  }) async {
    try {
      var params = {
        'query': query,
        'user_ids': userIds,
        'show_seat_info': showSeatInfo.toString(),
        'state': state,
      }..removeWhere((key, value) => value == null);
      var url = Uri.https(
        ApiConstants.baseUrl,
        urlTemplate(_Urls.allMembers, {':id': project.toString()}),
        params,
      );
      var response = await http.get(
        url,
        headers: {'Authorization': await AuthService.authHeader()},
      );
      if (response.statusCode == 200) {
        return usersFromJson(response.body);
      }
    } catch (e) {
      log(e.toString());
    }

    return [];
  }

  static Future<List<User>> getUsers(
    int project, {
    String? search,
    List<int>? skipUsers,
  }) async {
    try {
      var params = {
        'search': search,
        'skip_users[]': skipUsers?.map((e) => e.toString()).toList(),
      }..removeWhere((key, value) => value == null);
      var url = Uri.https(
        ApiConstants.baseUrl,
        urlTemplate(_Urls.projectUsers, {':id': project.toString()}),
        params,
      );
      var response = await http.get(
        url,
        headers: {'Authorization': await AuthService.authHeader()},
      );
      inspect(response);
      if (response.statusCode == 200) {
        return usersFromJson(response.body);
      }
    } catch (e) {
      print(e.toString());
    }

    return [];
  }
}
