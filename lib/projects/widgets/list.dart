import 'package:flutter/material.dart';
import 'package:gitlab/api_service.dart';
import 'package:gitlab/projects/project_model.dart';
import 'package:gitlab/projects/widgets/list_tile.dart';
import 'package:gitlab/widgets/loader/list.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class ProjectListWidget extends StatefulWidget {
  const ProjectListWidget({super.key});

  @override
  State<ProjectListWidget> createState() => ProjectListWidgetState();
}

class ProjectListWidgetState<T extends ProjectListWidget> extends State<T> {
  final pageSize = 20;
  final PagingController<int, Project> pagingController =
      PagingController(firstPageKey: 0);

  @override
  void initState() {
    super.initState();
    pagingController.addPageRequestListener((pageKey) => fetchPage(pageKey));
  }

  Future<void> fetchPage(int pageKey) async {
    try {
      var page = (pageKey / pageSize).floor() + 1;
      var list = await getProjects(page, pageSize);
      final isLastPage = pageKey + list.data.length == list.total;
      if (isLastPage) {
        pagingController.appendLastPage(list.data);
      } else {
        final nextPageKey = pageKey + list.data.length;
        pagingController.appendPage(list.data, nextPageKey);
      }
    } catch (error) {
      pagingController.error = error;
    }
  }

  Future<PaginatedAPIList<Project>> getProjects(int page, int size) async {
    return Project.list(
      page: page,
      perPage: pageSize,
    );
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () => Future.sync(() => pagingController.refresh()),
      child: PagedListView(
        pagingController: pagingController,
        builderDelegate: PagedChildBuilderDelegate<Project>(
          itemBuilder: (context, item, index) => ProjectListTile(project: item),
          firstPageProgressIndicatorBuilder: (context) => const SizedBox(
            width: double.infinity,
            height: 0,
            child: SkeletonListView(),
          ),
          newPageProgressIndicatorBuilder: (context) => const SizedBox(
            width: double.infinity,
            height: 72,
            child: SkeletonListView(itemCount: 1),
          ),
        ),
      ),
    );
  }
}
