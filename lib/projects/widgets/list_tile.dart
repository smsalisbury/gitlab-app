import 'package:flutter/material.dart';
import 'package:gitlab/api_service.dart';
import 'package:gitlab/pipelines/pipeline_model.dart';
import 'package:gitlab/projects/project_model.dart';

class ProjectListTile extends StatefulWidget {
  final Project project;

  const ProjectListTile({super.key, required this.project});

  @override
  State<ProjectListTile> createState() => _ProjectListTileState();
}

class _ProjectListTileState extends State<ProjectListTile> {
  Map pipelineIconMap = Map.from({
    'success': const Icon(Icons.check_circle_outline, color: Colors.green),
    'failed': const Icon(Icons.remove_circle_outline, color: Colors.redAccent),
  });
  late Pipeline? pipeline;

  @override
  void initState() {
    super.initState();
    pipeline = null;
    getData();
  }

  void getData() async {
    pipeline = (await ApiService().getLatestPipeline(widget.project.id));
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      // leading: ApiService().getProjectAvatar(widget.project),
      title: Text(
        widget.project.name,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      ),
      subtitle: widget.project.nameWithNamespace != null
          ? Text(
              widget.project.nameWithNamespace ?? '',
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            )
          : null,
      // trailing: const Icon(Icons.check_circle_outline),
      trailing: pipeline == null ? null : pipelineIconMap[pipeline!.status],
    );
  }
}
