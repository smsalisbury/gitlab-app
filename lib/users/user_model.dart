import 'dart:convert';
import 'dart:developer';
import 'package:http/http.dart' as http;

import '../auth_service.dart';
import '../constants.dart';

User userFromJson(String str) => User.fromJson(json.decode(str));
List<User> usersFromJson(String str) =>
    List<User>.from(json.decode(str).map((x) => User.fromJson(x)));
final Map<String, User> _cachedUsers = {};

class _Urls {
  static String list = '${ApiConstants.versionPath}/users';
}

class User {
  final int id;
  final String name;
  final String? avatarUrl;

  User({
    required this.id,
    required this.name,
    this.avatarUrl,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        avatarUrl: json["avatar_url"],
        name: json["name"],
      );

  static Future<User?> getMe({cached = true}) async {
    if (cached && _cachedUsers['me'] != null) return _cachedUsers['me'];
    try {
      var url = Uri.https(ApiConstants.baseUrl, ApiConstants.user);
      var response = await http
          .get(url, headers: {'Authorization': await AuthService.authHeader()});
      if (response.statusCode == 200) {
        User model = userFromJson(response.body);
        _cachedUsers['me'] = model;
        return model;
      }
    } catch (e) {
      log(e.toString());
    }
    return null;
  }

  static Future<List<User>> list({
    String? username,
    String? search,
    cached = true,
  }) async {
    try {
      Map<String, String?> params = {
        'search': search,
        'username': username,
      }..removeWhere((key, value) => value == null);
      var url = Uri.https(
        ApiConstants.baseUrl,
        _Urls.list,
        params,
      );
      var response = await http.get(
        url,
        headers: {'Authorization': await AuthService.authHeader()},
      );
      if (response.statusCode == 200) {
        List<User> model = usersFromJson(response.body);
        return model;
      }
    } catch (e) {
      log(e.toString());
    }
    return [];
  }

  static Future<User?> getByUsername(String username, {cached = true}) async {
    if (cached && _cachedUsers[username] != null) return _cachedUsers[username];
    var list = await User.list(username: username);
    return list.isNotEmpty ? list[0] : null;
  }

  static Future<User?> getByEmail(String email, {cached = true}) async {
    if (cached && _cachedUsers[email] != null) return _cachedUsers[email];
    var list = await User.list(search: email);
    return list.isNotEmpty ? list[0] : null;
  }
}
