import 'package:flutter/material.dart';
import 'package:gitlab/users/user_model.dart';

import '../../projects/project_model.dart';

class UserAvatar extends StatelessWidget {
  final User user;
  final double opacity;
  final double? size;

  const UserAvatar({
    super.key,
    required this.user,
    this.opacity = 1.0,
    this.size,
  });

  @override
  Widget build(BuildContext context) {
    var radius = size == null ? null : size! / 2;
    return Opacity(
      opacity: opacity,
      child: user.avatarUrl != null
          ? CircleAvatar(
              foregroundImage: NetworkImage(
                user.avatarUrl ?? '',
                headers: {},
              ),
              radius: radius,
            )
          : CircleAvatar(
              radius: radius,
              child: Text(user.name[0].toUpperCase()),
            ),
    );
  }
}

class UserAvatarByName extends StatefulWidget {
  final String name;
  final int project;

  const UserAvatarByName(this.name, this.project, {super.key});

  @override
  State<UserAvatarByName> createState() => _UserAvatarByNameState();
}

class _UserAvatarByNameState extends State<UserAvatarByName> {
  late Future<User?> _user;

  @override
  void initState() {
    _user = Project.getUserByName(widget.name, widget.project);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _user,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return UserAvatar(user: snapshot.data!);
        } else {
          return const CircleAvatar();
        }
      },
    );
  }
}
