import 'package:flutter/material.dart';
import 'package:gitlab/users/widgets/avatar.dart';

import '../user_model.dart';

class UserListTile extends StatelessWidget {
  final User user;
  final Widget? trailing;
  final void Function()? onTap;

  const UserListTile(
    this.user, {
    this.onTap,
    this.trailing,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: UserAvatar(user: user),
      title: Text(user.name),
      onTap: onTap,
      trailing: trailing,
    );
  }
}
