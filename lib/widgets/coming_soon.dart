import 'package:flutter/material.dart';

class ComingSoonLabel extends StatelessWidget {
  final Widget child;
  const ComingSoonLabel({super.key, required this.child});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        child,
        const SizedBox(width: 8),
        Text(
          'Coming soon!'.toUpperCase(),
          style: Theme.of(context).textTheme.bodySmall?.copyWith(
                color: Theme.of(context).colorScheme.error,
              ),
        ),
      ],
    );
  }
}
