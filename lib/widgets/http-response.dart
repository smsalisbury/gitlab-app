import 'package:flutter/material.dart';
import 'package:http/http.dart';

class HttpResponse extends StatelessWidget {
  final Response response;
  const HttpResponse(this.response, {super.key});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Status code: ${response.statusCode}'),
            Text(response.body),
          ],
        ),
      ),
    );
  }
}
