
import 'package:flutter/material.dart';
import 'package:gitlab/widgets/labels/label_model.dart';

class LabelFromModel extends StatelessWidget {
  final height = 20;
  final LabelModel label;
  const LabelFromModel(this.label, {super.key});

  @override
  Widget build(BuildContext context) {
    var parts = label.name.split('::');
    var text = parts[0];
    var detail = parts.length > 1 ? parts[1] : null;
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: label.color),
        borderRadius: BorderRadius.all(Radius.circular(height * 0.5)),
      ),
      clipBehavior: Clip.hardEdge,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            padding: detail == null
                ? EdgeInsets.symmetric(
                    horizontal: height * 0.4,
                    vertical: height * 0.1,
                  )
                : EdgeInsets.only(
                    left: height * 0.4,
                    bottom: height * 0.1,
                    top: height * 0.1,
                    right: height * 0.2,
                  ),
            color: label.color,
            child: Text(
              text,
              style: TextStyle(
                fontSize: height * 0.5,
                color: label.textColor,
              ),
            ),
          ),
          if (detail != null)
            Container(
              padding: EdgeInsets.only(
                right: height * 0.4,
                bottom: height * 0.1,
                top: height * 0.1,
                left: height * 0.2,
              ),
              child: Text(
                detail,
                style: TextStyle(fontSize: height * 0.5),
              ),
            )
        ],
      ),
    );
  }
}

class Label extends StatefulWidget {
  final String name;
  final int? projectId;

  const Label(this.name, {super.key, this.projectId});

  @override
  State<Label> createState() => _LabelState();

  String get cleanName {
    return name.replaceAll('"', '').replaceAll('~', '');
  }
}

class _LabelState extends State<Label> {
  final height = 20;
  late LabelModel label = LabelModel(
    id: 0,
    name: widget.cleanName,
    color: Colors.grey,
    textColor: Colors.black,
  );
  Color color = Colors.grey;
  Color textColor = Colors.black;

  @override
  void initState() {
    super.initState();
    getData();
  }

  void getData() async {
    if (widget.projectId != null) {
      var labelModel =
          await LabelModel.getLabel(widget.projectId!, widget.cleanName);
      if (labelModel != null) {
        label = labelModel;
      }
    }

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return LabelFromModel(label);
  }
}
