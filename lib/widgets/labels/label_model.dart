import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:gitlab/api_service.dart';
import 'package:http/http.dart' as http;

import '../../auth_service.dart';
import '../../constants.dart';

LabelModel labelFromJson(String str) => LabelModel.fromJson(json.decode(str));
List<LabelModel> labelsFromJson(String str) =>
    List<LabelModel>.from(json.decode(str).map((x) => LabelModel.fromJson(x)));

class _Urls {
  static String getOne =
      '${ApiConstants.versionPath}/projects/:id/labels/:label';
  static String list = '${ApiConstants.versionPath}/projects/:id/labels';
}

final Map<String, LabelModel?> cachedLabels = {};

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    final buffer = StringBuffer();
    if (hexColor.length == 6 || hexColor.length == 7) buffer.write('ff');
    buffer.write(hexColor.replaceFirst('#', ''));
    return int.parse(buffer.toString(), radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}

class LabelModel {
  final int id;
  final String name;
  final Color color;
  final Color textColor;

  LabelModel({
    required this.id,
    required this.name,
    required this.color,
    required this.textColor,
  });

  factory LabelModel.fromJson(Map<String, dynamic> json) => LabelModel(
        id: json["id"],
        name: json["name"],
        color: HexColor(json["color"]),
        textColor: HexColor(json["text_color"]),
      );

  static Future<PaginatedAPIList<LabelModel>> listForProject(
    int projectId, {
    bool? withCounts,
    bool? includeAncestorGroups,
    String? search,
  }) async {
    try {
      var params = {
        "with_counts": withCounts?.toString(),
        "include_ancestor_groups": includeAncestorGroups?.toString(),
        "search": search,
      }..removeWhere((key, value) => value == null);
      var url = Uri.https(
        ApiConstants.baseUrl,
        urlTemplate(_Urls.list, {
          ':id': projectId.toString(),
        }),
        params,
      );
      var response = await http.get(
        url,
        headers: {'Authorization': await AuthService.authHeader()},
      );
      if (response.statusCode == 200) {
        List<LabelModel> data = labelsFromJson(response.body);
        return PaginatedAPIList<LabelModel>(
          data,
          response.headers.containsKey('x-total')
              ? int.parse(response.headers['x-total']!)
              : data.length,
        );
      }
    } catch (e) {
      print(e.toString());
    }
    return PaginatedAPIList.empty();
  }

  static Future<LabelModel?> getLabel(
    int project,
    String name, {
    cached = true,
  }) async {
    var cacheKey = project.toString() + name;
    if (cached && cachedLabels.containsKey(cacheKey)) {
      return cachedLabels[cacheKey];
    }

    try {
      var url = Uri.https(
        ApiConstants.baseUrl,
        urlTemplate(_Urls.getOne, {
          ':id': project.toString(),
          ':label': name,
        }),
      );
      var response = await http
          .get(url, headers: {'Authorization': await AuthService.authHeader()});
      if (response.statusCode == 200) {
        LabelModel model = labelFromJson(response.body);
        cachedLabels[cacheKey] = model;
        inspect(model);
        return model;
      }
    } catch (e) {
      print(e.toString());
    }
    return null;
  }
}
