import 'package:flutter/material.dart';
import 'package:gitlab/widgets/loader/shapes.dart';

import 'skeleton.dart';

class SkeletonListView extends StatelessWidget {
  final Widget? item;
  final Widget Function(BuildContext, int)? itemBuilder;
  final int? itemCount;

  const SkeletonListView({
    Key? key,
    this.item,
    this.itemBuilder,
    this.itemCount,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SkeletonItem(
      child: ListView.builder(
        physics: const NeverScrollableScrollPhysics(),
        itemCount: itemCount,
        itemBuilder:
            itemBuilder ?? (context, index) => item ?? const SkeletonListTile(),
      ),
    );
  }
}

class SkeletonListTile extends StatelessWidget {
  final Widget leading;
  final Widget title;
  final Widget subtitle;
  final Widget? trailing;

  // final SkeletonListTileStyle style;

  const SkeletonListTile({
    Key? key,
    this.leading = const CircleAvatar(),
    this.title = const SkeletonLine(),
    this.subtitle = const SkeletonLine(randomLength: true),
    this.trailing,
  }) : super(key: key);
  // : assert(height >= lineHeight + spacing + (padding?.vertical ?? 16) + 2);

  @override
  Widget build(BuildContext context) {
    return SkeletonItem(
      child: ListTile(
        leading: leading,
        title: title,
        subtitle: subtitle,
        trailing: trailing,
      ),
    );
  }
}
