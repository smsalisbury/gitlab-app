import 'dart:math';
import 'package:flutter/material.dart';

import 'skeleton.dart';

class SkeletonAvatar extends StatelessWidget {
  final double? radius;
  const SkeletonAvatar({Key? key, this.radius}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SkeletonItem(child: CircleAvatar(radius: radius));
  }
}

class SkeletonIcon extends StatelessWidget {
  final Icon icon;
  const SkeletonIcon({Key? key, this.icon = const Icon(Icons.circle)})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SkeletonItem(child: icon);
  }
}

class SkeletonLine extends StatelessWidget {
  final double width;
  final double? height;
  final bool? randomLength;
  final double? minLength;
  final double? maxLength;
  final AlignmentGeometry alignment;

  const SkeletonLine({
    Key? key,
    this.width = double.infinity,
    this.height,
    this.randomLength,
    this.minLength,
    this.maxLength,
    this.alignment = AlignmentDirectional.centerStart,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SkeletonItem(
      child: Align(
        alignment: alignment,
        child: LayoutBuilder(
          builder: (context, constraints) {
            return Container(
              width: ((randomLength != null && randomLength!) ||
                      (randomLength == null &&
                          (minLength != null && maxLength != null)))
                  ? doubleInRange(
                      minLength ?? ((maxLength ?? constraints.maxWidth) / 3),
                      maxLength ?? constraints.maxWidth)
                  : width,
              height: height ??
                  Theme.of(context).textTheme.bodySmall?.fontSize ??
                  16,
              decoration: BoxDecoration(
                color: Theme.of(context).colorScheme.background,
                borderRadius: BorderRadius.circular(16),
              ),
            );
          },
        ),
      ),
    );
  }
}

class SkeletonParagraph extends StatelessWidget {
  final int lines;

  const SkeletonParagraph({
    Key? key,
    this.lines = 5,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SkeletonItem(
      child: Column(
        children: [
          for (var i = 1; i <= lines; i++) ...[
            SkeletonLine(randomLength: i == lines),
            if (i != lines) const SizedBox(height: 12)
          ]
        ],
      ),
    );
  }
}

double doubleInRange(num start, num end) =>
    Random().nextDouble() * (end - start) + start;
