import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart' as html;
import 'package:gitlab/constants.dart';
import 'package:gitlab/projects/project_model.dart';
import 'package:gitlab/widgets/labels/label.dart';
import 'package:gitlab/widgets/loader/shapes.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../api_service.dart';
import '../../issues/widgets/detail/detail.dart';

class GitLabMarkdown extends StatefulWidget {
  final String data;
  final int? projectId;
  final String? projectPath;
  final Widget? loader;
  const GitLabMarkdown(
    this.data, {
    super.key,
    this.projectId,
    this.projectPath,
    this.loader,
  });

  @override
  State<GitLabMarkdown> createState() => _GitLabMarkdownState();
}

class _GitLabMarkdownState extends State<GitLabMarkdown> {
  late Future<String> _rendered;
  late Project? project;

  @override
  void initState() {
    project = null;
    _rendered = _getProjectPath().then(
        (path) => ApiService.renderMarkdown(widget.data, projectPath: path));
    super.initState();
  }

  Future<String?> _getProjectPath() {
    if (widget.projectId != null || widget.projectPath != null) {
      return Project.get(
        id: widget.projectId,
        path: widget.projectPath,
      ).then((value) {
        project = value;
        return value?.pathWithNamespace;
      });
    }
    return Future.value(null);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _rendered,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return html.HtmlWidget(
            snapshot.data!,
            factoryBuilder: () =>
                _GitLabWidgetFactory(context: context, project: project),
            onTapUrl: (href) async {
              var uri = Uri.parse(href);
              if (uri.host == ApiConstants.baseUrl) {
                return _navigateGitlabLink(uri, context);
              }
              return launchUrl(uri, mode: LaunchMode.externalApplication);
            },
          );
        }
        return widget.loader ?? const SkeletonParagraph();
      },
    );
  }
}

Future<bool> _navigateGitlabLink(
  Uri target,
  BuildContext context,
) async {
  // Show some pages in-app
  var segments = [...target.pathSegments];
  if (segments[segments.length - 2] == 'issues') {
    var iid = int.parse(segments.removeLast());
    segments.removeLast();
    segments.removeWhere((element) => element == '-');
    Navigator.push(
        context, IssueDetailRoute(iid: iid, projectPath: segments.join('/')));
    return true;
  }

  // Open externally
  return launchUrl(target, mode: LaunchMode.externalApplication);
}

class _GitLabWidgetFactory extends html.WidgetFactory {
  final BuildContext context;
  final Project? project;

  _GitLabWidgetFactory({
    required this.context,
    this.project,
  });

  @override
  void parse(html.BuildMetadata meta) {
    final element = meta.element;

    // Render labels
    if (element.classes.contains('gfm-label')) {
      meta.register(html.BuildOp(
        onTree: (meta, tree) {
          html.WidgetBit.inline(
            tree.parent!,
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Label(
                  element.attributes['data-original'] ?? element.text,
                  projectId: project?.id,
                ),
              ],
            ),
          ).insertBefore(tree);
          tree.detach();
        },
      ));

      return;
    }

    return super.parse(meta);
  }
}
