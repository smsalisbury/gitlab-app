import 'package:flutter/material.dart';
import 'package:gitlab/widgets/markdown/gitlab_markdown.dart';
import 'package:material_symbols_icons/symbols.dart';

class MarkdownField extends StatefulWidget {
  final List<Widget>? actions;
  final TextEditingController? controller;
  final int? maxLines;
  final int? minLines;
  final InputDecoration? decoration;
  final bool expands;
  final bool showSend;
  final bool autofocus;
  final String? initialValue;
  final void Function(String? value)? onSaved;
  final Future<void> Function(String value)? onSubmit;

  const MarkdownField({
    super.key,
    this.actions,
    this.controller,
    this.maxLines,
    this.minLines,
    this.autofocus = false,
    this.showSend = true,
    this.onSubmit,
    this.decoration,
    this.expands = false,
    this.initialValue,
    this.onSaved,
  });

  @override
  State<MarkdownField> createState() => MarkdownFieldState();
}

class MarkdownFieldState extends State<MarkdownField> {
  late TextEditingController controller;
  late UndoHistoryController undoController = UndoHistoryController();
  Color borderColor = Colors.black;
  bool preview = false;
  late FocusNode focusNode;
  bool _sendable = false;
  bool _sending = false;

  @override
  void initState() {
    focusNode = FocusNode();
    controller = widget.controller ?? TextEditingController();
    controller.text = widget.initialValue ?? '';
    super.initState();
  }

  @override
  void dispose() {
    focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return InputDecorator(
      decoration: (widget.decoration ?? const InputDecoration()).copyWith(
        border: const OutlineInputBorder(),
        floatingLabelBehavior: FloatingLabelBehavior.auto,
        contentPadding: EdgeInsets.zero,
      ),
      isFocused: focusNode.hasFocus,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          widget.expands
              ? Expanded(
                  child: _buildContent(),
                )
              : _buildContent(),
          Container(
            height: 44,
            padding: const EdgeInsets.symmetric(horizontal: 2),
            child: _buildControls(),
          ),
        ],
      ),
    );
  }

  Widget _buildContent() {
    return preview
        ? widget.expands
            ? Flex(
                direction: Axis.vertical,
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(12),
                      child: GitLabMarkdown(controller.text),
                    ),
                  ),
                  const Divider(),
                ],
              )
            : Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(12),
                    child: GitLabMarkdown(controller.text),
                  ),
                  const Divider()
                ],
              )
        : TextFormField(
            autofocus: widget.autofocus,
            controller: controller,
            expands: widget.expands,
            textCapitalization: TextCapitalization.sentences,
            decoration: const InputDecoration(
              contentPadding: EdgeInsets.fromLTRB(12, 16, 12, 16),
            ),
            focusNode: focusNode,
            keyboardType: TextInputType.multiline,
            maxLines: widget.maxLines,
            minLines: widget.minLines,
            onChanged: (value) => setState(() {
              _sendable = value.isNotEmpty;
            }),
            onSaved: widget.onSaved,
          );
  }

  Widget _buildControls() {
    return Row(
      children: [
        Flexible(
          child: ListView(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            children: _actions,
          ),
        ),
        if (widget.showSend)
          IconButton(
            onPressed: _sendable && !_sending && widget.onSubmit != null
                ? _send
                : null,
            color: Theme.of(context).colorScheme.primary,
            icon: _sending
                ? SizedBox(
                    width: Theme.of(context).iconTheme.size ?? 24,
                    height: Theme.of(context).iconTheme.size ?? 24,
                    child: const CircularProgressIndicator(),
                  )
                : const Icon(Icons.send),
          ),
      ],
    );
  }

  Future<void> _send() async {
    if (widget.onSubmit == null) return;
    setState(() {
      _sending = true;
    });
    await widget.onSubmit!(controller.text);
    setState(() {
      _sending = false;
    });
  }

  void togglePreview() {
    setState(() {
      preview = !preview;
    });
  }

  static MarkdownFieldState of(BuildContext context) {
    final MarkdownFieldState? result =
        context.findAncestorStateOfType<MarkdownFieldState>();
    if (result != null) return result;
    throw FlutterError(
        'MarkdownField.of() called with a context that does not contain a MarkdownField.');
  }

  List<Widget> get _actions {
    return MarkdownAction.all;
  }
}

class MarkdownAction extends StatefulWidget {
  final Icon icon;
  final bool disableable;
  final void Function(MarkdownFieldState state)? onTap;
  final List<MarkdownAction>? children;

  const MarkdownAction({
    super.key,
    required this.icon,
    this.onTap,
    this.children,
    this.disableable = true,
  });

  static List<MarkdownAction> get all {
    return [
      MarkdownAction.preview,
      MarkdownAction.bold,
      MarkdownAction.italic,
      MarkdownAction.strikethrough,
      MarkdownAction.link,
      MarkdownAction.title,
      MarkdownAction.ul,
      MarkdownAction.code,
      MarkdownAction.quote,
      MarkdownAction.undo,
    ];
  }

  static MarkdownAction get preview {
    return MarkdownAction(
      disableable: false,
      onTap: (state) => state.togglePreview(),
      icon: const Icon(Symbols.visibility_rounded),
    );
  }

  static MarkdownAction get undo {
    return MarkdownAction(
      onTap: (state) => state.undoController.undo(),
      icon: const Icon(Symbols.undo_rounded),
    );
  }

  static MarkdownAction get bold {
    return MarkdownAction(
      onTap: (state) =>
          MarkdownAction._toggleSelectionWrap(state.controller, wrapStr: '**'),
      icon: const Icon(Symbols.format_bold_rounded),
    );
  }

  static MarkdownAction get italic {
    return MarkdownAction(
      onTap: (state) =>
          MarkdownAction._toggleSelectionWrap(state.controller, wrapStr: '_'),
      icon: const Icon(Symbols.format_italic_rounded),
    );
  }

  static MarkdownAction get strikethrough {
    return MarkdownAction(
      onTap: (state) =>
          MarkdownAction._toggleSelectionWrap(state.controller, wrapStr: '~~'),
      icon: const Icon(Symbols.format_strikethrough_rounded),
    );
  }

  static MarkdownAction get link {
    return MarkdownAction(
      onTap: (state) => MarkdownAction._toggleSelectionWrap(
        state.controller,
        prefixStr: '[',
        suffixStr: ']()',
      ),
      icon: const Icon(Symbols.link_rounded),
    );
  }

  static MarkdownAction get quote {
    return const MarkdownAction(
      icon: Icon(Symbols.format_quote),
    );
  }

  static MarkdownAction get code {
    return const MarkdownAction(
      icon: Icon(Symbols.code),
    );
  }

  static MarkdownAction get ul {
    return const MarkdownAction(
      icon: Icon(Symbols.format_list_bulleted),
    );
  }

  static MarkdownAction get title {
    return MarkdownAction(
      icon: const Icon(Symbols.format_h1_rounded),
      children: [
        MarkdownAction(
          onTap: (state) =>
              MarkdownAction._toggleHeader(state.controller, level: 1),
          icon: const Icon(Symbols.format_h1_rounded),
        ),
        MarkdownAction(
          onTap: (state) =>
              MarkdownAction._toggleHeader(state.controller, level: 2),
          icon: const Icon(Symbols.format_h2_rounded),
        ),
        MarkdownAction(
          onTap: (state) =>
              MarkdownAction._toggleHeader(state.controller, level: 3),
          icon: const Icon(Symbols.format_h3_rounded),
        ),
        MarkdownAction(
          onTap: (state) =>
              MarkdownAction._toggleHeader(state.controller, level: 4),
          icon: const Icon(Symbols.format_h4_rounded),
        ),
        MarkdownAction(
          onTap: (state) =>
              MarkdownAction._toggleHeader(state.controller, level: 5),
          icon: const Icon(Symbols.format_h5_rounded),
        ),
        MarkdownAction(
          onTap: (state) =>
              MarkdownAction._toggleHeader(state.controller, level: 6),
          icon: const Icon(Symbols.format_h6_rounded),
        ),
      ],
    );
  }

  @override
  State<MarkdownAction> createState() => _MarkdownActionState();

  static void _toggleSelectionWrap(
    TextEditingController controller, {
    String? wrapStr,
    String? prefixStr,
    String? suffixStr,
  }) {
    // Default for strings
    assert(wrapStr != null || prefixStr != null);
    prefixStr = prefixStr ?? wrapStr as String;
    suffixStr = suffixStr ?? prefixStr;

    // Select text if inside a word
    if (controller.selection.isValid) {
      var wordStart = controller.selection
              .textBefore(controller.text)
              .lastIndexOf(RegExp(r'\s')) +
          1;
      var wordEnd = controller.selection
              .textAfter(controller.text)
              .indexOf(RegExp(r'(?:$|\s)')) +
          controller.selection.extentOffset;
      var newSelection =
          TextSelection(baseOffset: wordStart, extentOffset: wordEnd);
      if (controller.isSelectionWithinTextBounds(newSelection)) {
        controller.selection = newSelection;
      }
    }

    // Rewrite the text
    final prefix = controller.selection.textBefore(controller.text);
    final selectedText = controller.selection.textInside(controller.text);
    final suffix = controller.selection.textAfter(controller.text);
    final alreadyWrapped =
        selectedText.substring(0, prefixStr.length) == prefixStr &&
            selectedText.substring(selectedText.length - suffixStr.length) ==
                suffixStr;
    final newSelectedText = alreadyWrapped
        ? selectedText.substring(
            prefixStr.length, selectedText.length - suffixStr.length)
        : '$prefixStr$selectedText$suffixStr';
    final updatedText = '$prefix$newSelectedText$suffix';

    // Update the controller value
    var baseChange = (alreadyWrapped ? 0 : 1) * prefixStr.length;
    var extentChange = alreadyWrapped
        ? -1 * suffixStr.length - prefixStr.length
        : 1 * prefixStr.length;
    var selection = TextSelection(
      baseOffset: controller.selection.baseOffset + baseChange,
      extentOffset: controller.selection.extentOffset + extentChange,
    );
    controller.value = controller.value.copyWith(
      text: updatedText,
      selection: selection,
    );
  }

  static void _toggleHeader(
    TextEditingController controller, {
    required int level,
  }) {
    assert(level >= 1 && level <= 6);
    if (!controller.selection.isValid) return;
    var lineStart = controller.selection
            .textBefore(controller.text)
            .lastIndexOf(RegExp(r'\n')) +
        1;
    var existingRegex = RegExp('^#* ');
    var before = controller.text.substring(0, lineStart);
    var after = controller.text.substring(lineStart);
    var existing = existingRegex.firstMatch(after);
    var headerSymbol = '${'#' * level} ';
    var offsetChange = headerSymbol.length -
        (existing == null ? 0 : (existing.end - existing.start));
    controller.value = controller.value.copyWith(
      text: "$before$headerSymbol${after.replaceFirst(existingRegex, '')}",
      selection: TextSelection(
        baseOffset: controller.selection.baseOffset + offsetChange,
        extentOffset: controller.selection.extentOffset + offsetChange,
      ),
    );
  }
}

class _MarkdownActionState extends State<MarkdownAction> {
  bool opened = false;

  @override
  Widget build(BuildContext context) {
    return opened
        ? Padding(
            padding: const EdgeInsets.symmetric(vertical: 4),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                color: Theme.of(context).colorScheme.surfaceVariant,
              ),
              child: Theme(
                data: ThemeData(
                  iconTheme: const IconThemeData(size: 20),
                  visualDensity: VisualDensity.compact,
                ),
                child: Row(
                  children: [
                    ...?widget.children,
                    IconButton(
                      onPressed: () => setState(() {
                        opened = false;
                      }),
                      icon: const Icon(Symbols.chevron_left),
                    )
                  ],
                ),
              ),
            ),
          )
        : IconButton(
            onPressed: () => _disabled ? null : _onPressedAction(context),
            icon: widget.icon,
          );
  }

  MarkdownFieldState get _fieldState {
    return MarkdownFieldState.of(context);
  }

  bool get _disabled {
    return widget.disableable ? _fieldState.preview : false;
  }

  void _onPressedAction(context) {
    if (widget.children == null) {
      if (widget.onTap != null) widget.onTap!(_fieldState);
    } else {
      setState(() {
        opened = true;
      });
    }
  }
}
